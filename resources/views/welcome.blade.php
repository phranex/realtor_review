@extends('layouts.master')

@push('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<style>
.nice-select,.nice-select .list{
    width: 100%;
}

</style>
@endpush

@section('content')
    <main>
        <section class="hero_single version_2">
            <div class="wrapper">
                <div class="container">
                    <div class="row justify-content-center pt-lg-5">
                        <div class="col-xl-5 col-lg-6">
                            <h3>{{@$index['banner-title-lg']}}</h3>
                            <p>{{@$index['banner-title-sm']}}</p>
                            <form method="get" action="{{route('review.search')}}">
                                @csrf
                                <div class="custom-search-input-2">
                                    <div class="form-group">
                                            {{-- <select multiple class="search-realtor" name="">
                                                <option value=''>Search a Realtor</option>
                                            </select> --}}
                                        <input class="form-control search-realtor" type="text" name='realtor' placeholder="Search Pro by name">
                                        <i class="icon_search"></i>
                                    </div>

                                    <input type="submit" value="Search">
                                </div>
                            </form>
                        </div>
                        <div class="col-xl-5 col-lg-6 text-right d-none d-lg-block">
                            <img src="img/graphic_home_2.svg" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /hero_single -->

        {{-- <div class="container margin_60_35">
            <div class="main_title_3">
                <h2>Top Categories</h2>
                <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
                <a href="all-categories.html">View all</a>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-3 col-6">
                    <a href="grid-listings-filterstop.html" class="box_cat_home">
                        <img src="img/icon_home_1.svg" width="65" height="65" alt="">
                        <h3>Clothing</h3>
                        <ul class="clearfix">
                            <li><strong>1,023</strong> Results</li>
                            <li><strong>2,435</strong><i class="icon-comment"></i></li>
                        </ul>
                    </a>
                </div>
                <div class="col-lg-3 col-6">
                    <a href="grid-listings-filterstop.html" class="box_cat_home">
                        <img src="img/icon_home_2.svg" width="65" height="65" alt="">
                        <h3>Hotels</h3>
                        <ul class="clearfix">
                            <li><strong>856</strong> Results</li>
                            <li><strong>455</strong><i class="icon-comment"></i></li>
                        </ul>
                    </a>
                </div>
                <div class="col-lg-3 col-6">
                    <a href="grid-listings-filterstop.html" class="box_cat_home">
                        <img src="img/icon_home_3.svg" width="65" height="65" alt="">
                        <h3>Restaurants</h3>
                        <ul class="clearfix">
                            <li><strong>2,400</strong> Results</li>
                            <li><strong>1,323</strong><i class="icon-comment"></i></li>
                        </ul>
                    </a>
                </div>
                <div class="col-lg-3 col-6">
                    <a href="grid-listings-filterstop.html" class="box_cat_home">
                        <img src="img/icon_home_4.svg" width="65" height="65" alt="">
                        <h3>Bars</h3>
                        <ul class="clearfix">
                            <li><strong>854</strong> Results</li>
                            <li><strong>345</strong><i class="icon-comment"></i></li>
                        </ul>
                    </a>
                </div>
                <div class="col-lg-3 col-6">
                    <a href="grid-listings-filterstop.html" class="box_cat_home">
                        <img src="img/icon_home_8.svg" width="65" height="65" alt="">
                        <h3>Electronics</h3>
                        <ul class="clearfix">
                            <li><strong>1,210</strong> Results</li>
                            <li><strong>530</strong><i class="icon-comment"></i></li>
                        </ul>
                    </a>
                </div>
                <div class="col-lg-3 col-6">
                    <a href="grid-listings-filterstop.html" class="box_cat_home">
                        <img src="img/icon_home_5.svg" width="65" height="65" alt="">
                        <h3>Beauty</h3>
                        <ul class="clearfix">
                            <li><strong>1,343</strong> Results</li>
                            <li><strong>315</strong><i class="icon-comment"></i></li>
                        </ul>
                    </a>
                </div>
                <div class="col-lg-3 col-6">
                    <a href="grid-listings-filterstop.html" class="box_cat_home">
                        <img src="img/icon_home_6.svg" width="65" height="65" alt="">
                        <h3>Fitness</h3>
                        <ul class="clearfix">
                            <li><strong>678</strong> Results</li>
                            <li><strong>123</strong><i class="icon-comment"></i></li>
                        </ul>
                    </a>
                </div>
                <div class="col-lg-3 col-6">
                    <a href="grid-listings-filterstop.html" class="box_cat_home">
                        <img src="img/icon_home_7.svg" width="65" height="65" alt="">
                        <h3>Doctors</h3>
                        <ul class="clearfix">
                            <li><strong>378</strong> Results</li>
                            <li><strong>560</strong><i class="icon-comment"></i></li>
                        </ul>
                    </a>
                </div>
            </div>
        </div> --}}
        <!-- /container -->
        @isset($reviews)
            <div class="bg_color_1">
                <div class="container margin_60">
                    <div class="main_title_3">
                        <h2>Latest Reviews</h2>
                        {{-- <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p> --}}
                    <a href="{{route('review.show-random')}}">{{__('View all')}}</a>
                    </div>
                    @if($reviews)
                    <div id="reccomended" class="owl-carousel owl-theme">
                        @foreach ($reviews as $review)
                            <div class="item">
                                <div class="review_listing">
                                    <div class="clearfix">
                                        <figure><img src="img/avatar1.jpg" alt=""></figure>
                                        <span class="rating">
                                            {{rating($review['rating_score'])}}
                                        <small>{{__('Pro')}}</small>
                                    </div>
                                    <h3><strong>
                                            @if(!empty($review['contact']))
                                                {{ucwords($review['contact']['fullname'])}}
                                            @else
                                            {{ucwords($review['fullname'])}}
                                            @endif
                                        </strong> {{__('reviewed')}} <a href="{{route('user.view',$review['business_name_slug'] )}}"> {{ucwords($review['business_name'])}}</a></h3>
                                        <h4>"{{$review['title']}}"</h4>
                                    <p>{{substr($review['review'],0, 150)}} ...</p>
                                        <ul class="clearfix">
                                            <li><small>{{__('Published:')}} {{when($review['created_at'])}}</small></li>
                                            {{-- <li><a href="reviews-page.html" class="btn_1 small">Read review</a></li> --}}
                                        </ul>
                                </div>
                            </div>
                        @endforeach



                    </div>
                    @endif
                    <!-- /carousel -->
                </div>
                <!-- /container -->
            </div>
        @endisset

        <!-- /bg_color_1 -->

        {{-- <div class="call_section">
            <div class="container margin_80_55">
                <div class="row justify-content-center">
                    <div class="col-xl-5 col-lg-6">
                        <img alt="" class="img-fluid" src="img/graphic_home_1.svg">
                    </div>
                    <div class="col-xl-5 col-lg-6 pt-lg-5">
                        <h3>Let's Help You</h3>
                        <p>Realtor is a review platform open to everyone. Share your experiences to help others make better choices, and help companies up their game. Our mission is to bring people and companies together to create experiences for everyone.</p>
                        <p><a href="#sign-in-dialog" id="sign-in2" class="btn_1 add_top_10 wow bounceIn" title="Sign In">Join Realtor Now!</a></p>
                    </div>
                </div>
            </div>
            <!-- /container -->
        </div> --}}
        <!-- /call_section -->




    </main>


@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $(document).ready(function(){

            ('.search-realtor').select2({
                placeholder: 'Select an option'
            });
        });
    </script>

@endpush
