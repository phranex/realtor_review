@extends('layouts.master')


@section('content')
    <main>
        <div id="results">
            <div class="container">
                    <div class="row justify-content-between">
                            <div class="col-lg-3 col-md-4 col-10">
                            <h1><strong>{{count($results)}}</strong> {{__('Results')}} for "{{request('realtor')}}"</h1>
                            </div>
                            <div class="col-xl-5 col-md-6 col-2">
                                <a href="#0" class="search_mob btn_search_mobile"></a> <!-- /open search panel -->
                                <form method="get" action="{{route('review.search')}}">
                                        @csrf
                                    <div class="row no-gutters custom-search-input-2 inner">

                                        <div class="col-lg-11">
                                            <div class="form-group">
                                            <input class="form-control" type="text" value="{{request('realtor')}}" name="realtor" placeholder="Search reviews for a company">
                                                {{-- <i class="icon_search"></i> --}}
                                            </div>
                                        </div>
                                        {{-- <div class="col-lg-4">
                                            <select class="wide">
                                                <option>All Categories</option>
                                                <option>Shops</option>
                                                <option>Hotels</option>
                                                <option>Restaurants</option>
                                                <option>Bars</option>
                                                <option>Events</option>
                                                <option>Fitness</option>
                                            </select>
                                        </div> --}}
                                        <div class="col-xl-1 col-lg-1">
                                            <input type="submit" value="Search">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                <!-- /row -->
                {{-- <div class="search_mob_wp">
                    <div class="custom-search-input-2">
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Search reviews...">
                            <i class="icon_search"></i>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Where">
                            <i class="icon_pin_alt"></i>
                        </div>
                        <select class="wide">
                            <option>All Categories</option>
                            <option>Shops</option>
                            <option>Hotels</option>
                            <option>Restaurants</option>
                            <option>Bars</option>
                            <option>Events</option>
                            <option>Fitness</option>
                        </select>
                        <input type="submit" value="Search">
                    </div>
                </div> --}}
                <!-- /search_mobile -->
            </div>
            <!-- /container -->
        </div>
        <!-- /results -->
		<div class="container margin_60_35">

			<div class="isotope-wrapper">
                @isset($results)
                    @if(count($results))
                        @foreach($results as $result)
                            <div class="company_listing isotope-item high">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="company_info">
                                        <figure><a href="{{route('user.view',str_replace(' ', '_',$result['profile']['business_name']))}}"><img src="{{$result['avatar']}}" alt=""></a></figure>
                                        <h3>{{ucwords($result['fullname'])}} <small style="font-size:x-small">{{ucwords($result['profile']['business_name'])}}</small></h3>
                                        <p>
                                            @isset($result['description'])
                                                {{$result['description']}}
                                            @else
                                                No description.

                                            @endisset
                                        </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="text-center float-lg-right">
                                        <span class="rating"><strong>{{__("Based on ".$result['profile']['reviews']['count']." reviews")}}</strong>
                                            {{rating($result['profile']['reviews']['average_rating'])}}
                                        </span>
                                        <a href="{{route('user.view',str_replace(' ','_',$result['profile']['business_name']))}}" class="btn_1 small">{{__('View')}}</a>
                                        {{-- <a href="{{route('review.create',base64_encode($result['business_name']."-".$result['user']['data']['id']))}}" class="btn_1 small btn-xs">{{__('Review')}}</a> --}}

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                    <div class="company_listing isotope-item high">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="company_info alert text-center">
                                <img style="width:100px;display:block;margin:auto" src='{{asset('img/broker.png')}}' class="img-fluid" />
                                    <h3><strong>Oops! {{ucwords(request('realtor'))}}</strong> could not be found.</h3>
                                </div>
                            </div>
                        </div>
                    @endif
                <!-- /company_listing -->
                @else

                @endisset



			</div>
			<!-- /isotope-wrapper -->

			{{-- <p class="text-center"><a href="#0" class="btn_1 rounded add_top_15">Load more</a></p> --}}

		</div>
        <!-- /container -->

    </main>
    <!--/main-->
@endsection
