@extends('layouts.realtor')


@section('content')


    <div class="card card-statistics mb-0">
        <div class="card-header">
            <h4 class="card-title">Reviews</h4>
        </div>
        <div class="card-body">
            <div class="card-columns">
                @forelse($results['data'] as $result)
                <div class="card">

                    <div class="card-body">
                        <h4 class="card-title">{{$result['title']}}</h4>

                        <p class="card-text">{{$result['review']}}
                        </p>

                        <blockquote class="blockquote mb-0 card-body">
                            <div class="text-right">
                                <button  class="btn  btn-primary btn-xs">Reply</button>
                            </div>

                            <footer class="blockquote-footer">
                                <small class="text-muted">
                                    <cite title="Source Title">John Doe</cite>
                                </small>
                            </footer>
                        </blockquote>
                    </div>
                </div>
                @empty
                    <p>No review Available</p>
                    @endforelse

                <div class="card">

                    <div class="card-body">
                        <h4 class="card-title">Great Realtor</h4>
                        <p class="card-text">This is a great realtor. This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor</p>
                        <footer class="blockquote-footer">
                            <div class="text-right">
                                <button  class="btn  btn-primary btn-xs">Reply</button>
                            </div>
                            <small class="text-muted">
                                <cite title="Source Title">John Doe</cite>
                            </small>
                        </footer>
                        {{--<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>--}}
                    </div>
                </div>
                <div class="card bg-primary text-white text-center p-3">
                    <h4 class="card-title">Great Realtor</h4>
                    <blockquote class="blockquote mb-0">
                        <p class="text-white">
                            This is a great realtor. This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                        </p>
                        <footer class="blockquote-footer">
                            <div class="text-right">
                                <button  class="btn  btn-primary btn-xs">Reply</button>
                            </div>
                            <small class="text-muted">
                                <cite title="Source Title">John Doe</cite>
                            </small>
                        </footer>
                    </blockquote>
                </div>
                <div class="card text-center">
                    <div class="card-body">
                        <h4 class="card-title">Great Realtor</h4>
                        <p class="card-text">This is a great realtor. This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor</p>
                        <footer class="blockquote-footer">
                            <div class="text-right">
                                <button  class="btn  btn-primary btn-xs">Reply</button>
                            </div>
                            <small class="text-muted">
                                <cite title="Source Title">John Doe</cite>
                            </small>
                        </footer>
                        {{--<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>--}}
                    </div>
                </div>

                <div class="card p-3 text-right">
                    <h4 class="card-title">Great Realtor</h4>
                    <blockquote class="blockquote mb-0">
                        <p class="text-whit">
                            This is a great realtor. This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                        </p>
                        <footer class="blockquote-footer">
                            <div class="text-right">
                                <button  class="btn  btn-primary btn-xs">Reply</button>
                            </div>
                            <small class="text-muted">
                                <cite title="Source Title">John Doe</cite>
                            </small>
                        </footer>
                    </blockquote>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Powerful Performance</h4>
                        <p class="card-text">This is a great realtor. This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor</p>
                        <footer class="blockquote-footer">
                            <div class="text-right">
                                <button  class="btn  btn-primary btn-xs">Reply</button>
                            </div>
                            <small class="text-muted">
                                <cite title="Source Title">John Doe</cite>
                            </small>
                        </footer>
                        {{--<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>--}}
                    </div>
                </div>


                <div class="card">

                    <div class="card-body">
                        <h4 class="card-title">Great Realtor</h4>
                        <p class="card-text">This is a great realtor. This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                        </p>

                        <blockquote class="blockquote mb-0 card-body">
                            <div class="text-right">
                                <button  class="btn  btn-primary btn-xs">Reply</button>
                            </div>
                            <footer class="blockquote-footer">
                                <small class="text-muted">
                                    <cite title="Source Title">John Doe</cite>
                                </small>
                            </footer>
                        </blockquote>
                    </div>
                </div>

                <div class="card">

                    <div class="card-body">
                        <h4 class="card-title">Great Realtor</h4>
                        <p class="card-text">This is a great realtor. This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor</p>
                        <footer class="blockquote-footer">
                            <div class="text-right">
                                <button  class="btn  btn-primary btn-xs">Reply</button>
                            </div>
                            <small class="text-muted">
                                <cite title="Source Title">John Doe</cite>
                            </small>
                        </footer>
                        {{--<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>--}}
                    </div>
                </div>
                <div class="card bg-primary text-white text-center p-3">
                    <h4 class="card-title">Great Realtor</h4>
                    <blockquote class="blockquote mb-0">
                        <p class="text-white">
                            This is a great realtor. This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                        </p>
                        <footer class="blockquote-footer">
                            <div class="text-right">
                                <button  class="btn  btn-primary btn-xs">Reply</button>
                            </div>
                            <small class="text-muted">
                                <cite title="Source Title">John Doe</cite>
                            </small>
                        </footer>
                    </blockquote>
                </div>
                <div class="card text-center">
                    <div class="card-body">
                        <h4 class="card-title">Great Realtor</h4>
                        <p class="card-text">This is a great realtor. This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor</p>
                        <footer class="blockquote-footer">
                            <div class="text-right">
                                <button  class="btn  btn-primary btn-xs">Reply</button>
                            </div>
                            <small class="text-muted">
                                <cite title="Source Title">John Doe</cite>
                            </small>
                        </footer>
                        {{--<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>--}}
                    </div>
                </div>

                <div class="card p-3 text-right">
                    <h4 class="card-title">Great Realtor</h4>
                    <blockquote class="blockquote mb-0">
                        <p class="text-whit">
                            This is a great realtor. This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                        </p>
                        <footer class="blockquote-footer">
                            <div class="text-right">
                                <button  class="btn  btn-primary btn-xs">Reply</button>
                            </div>
                            <small class="text-muted">
                                <cite title="Source Title">John Doe</cite>
                            </small>
                        </footer>
                    </blockquote>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Powerful Performance</h4>
                        <p class="card-text">This is a great realtor. This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor
                            This is a great realtor</p>
                        <footer class="blockquote-footer">
                            <div class="text-right">
                                <button  class="btn  btn-primary btn-xs">Reply</button>
                            </div>
                            <small class="text-muted">
                                <cite title="Source Title">John Doe</cite>
                            </small>
                        </footer>
                        {{--<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
