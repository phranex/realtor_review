@extends('layouts.master')


@section('content')
    <main class="margin_main_container">
        <div class="container margin_60_35">
            <div class="row">
                    @isset($invitation)
                    @isset($invitation->review)
                    <div class="col-lg-8 m-auto">
                    <div class="review_card">
                            <div class="row">
                                <div class="col-md-2 user_info">
                                    <figure><img src="{{asset('img/avatar4.jpg')}}" alt=""></figure>
                                    <h5>{{__('Review:')}} "{{$invitation->user['first_name']}}  {{$invitation->user['last_name']}}"</h5>
                                </div>
                                <div class="col-md-10 review_content">
                                    <div class="clearfix add_bottom_15">
                                        <span class="rating">
                                            @if(@$invitation->review)
                                                @for($i = 0; $i < $invitation->review['rating_score'];  $i++)
                                                    <i class="icon_star"></i>
                                                @endfor
                                            @endif
                                            <em>{{number_format($invitation->review['rating_score'], 2)}}/5.00</em>
                                        </span>
                                        <em>{{__('Published:')}} {{when($invitation->review['created_at'])}}</em>
                                    </div>
                                    <h4>"{{$invitation->review['title']}}"</h4>
                                    <p>{{$invitation->review['review']}}</p>
                                    <ul >
                                        {{-- <li><a href="#0" class="btn_delete"><i class="icon-trash"></i>Delete</a></li> --}}
                                        <li style="margin-bottom:20px"><a href="#0"><i class="icon-edit-3"></i> Edit</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /row -->
                        </div>
                    </div>
                    @else
                    <div class="col-lg-12">
                    <div class="box_general write_review">
                            <form method="post" action="{{route('review.store')}}" enctype="multipart/form-data">
                                @csrf
                            <h1>{{__("Write a review of ")}}{{$invitation->user['first_name']}} {{$invitation->user['last_name']}}</h1>
                            <div class="rating_submit">
                                <div class="form-group">
                                    <label class="d-block">Overall rating</label>
                                    <span class="rating">


                                    <input type="radio" class="rating-input" id="5_star" required name="rating_score" value="5"><label for="5_star" class="rating-star"></label>
                                    <input type="radio" class="rating-input" id="4_star" required name="rating_score" value="4"><label for="4_star" class="rating-star"></label>
                                    <input type="radio" class="rating-input" id="3_star" required name="rating_score" value="3"><label for="3_star" class="rating-star"></label>
                                    <input type="radio" class="" style="opacity:0" required name="rating_score" >
                                    <input type="radio" class="rating-input" id="2_star" required name="rating_score" value="2"><label for="2_star" class="rating-star"></label>
                                    <input type="radio" class="rating-input" id="1_star" required name="rating_score" value="1"><label for="1_star" class="rating-star"></label>
                                </span>
                                </div>
                            </div>
                            <!-- /rating_submit -->
                            <div class="form-group">
                                <label>Title of your review</label>
                                <input class="form-control" type="text" required name="title" placeholder="If you could say it in one sentence, what would you say?">
                            </div>
                            <div class="form-group">
                                <label>Your review</label>
                                <textarea class="form-control" name="review" required style="height: 180px;" placeholder="Write your review to help others learn about this online business"></textarea>
                            </div>



                                <p class="text-muted">{{__('Your information is important to prevent double rating')}}</p>

                            <div class="form-group">
                                <label>Your Email</label>
                            <input class="form-control" value="{{$email}}" readonly type="email" name="email" placeholder="">

                            </div>
                            <div class="form-group">
                                <label>Your Full Name</label>
                                <input class="form-control" type="text" name="fullname" placeholder="">
                            </div>
                            {{-- <div class="form-group">
                                <label>Add your photo (optional)</label>
                                <div class="fileupload">
                                    <input type="file" name="picture" accept="image/*"></div>
                            </div> --}}
                            {{-- <div class="form-group">
                                <div class="checkboxes float-left add_bottom_15 add_top_15">
                                    <label class="container_check">Eos tollit ancillae ea, lorem consulatu qui ne, eu eros eirmod scaevola sea. Et nec tantas accusamus salutatus, sit commodo veritus te, erat legere fabulas has ut. Rebum laudem cum ea, ius essent fuisset ut. Viderer petentium cu his.
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div> --}}
                            <input type="hidden" name="contact_id" value="{{$invitation->contact['id']}}" />
                            <input type="hidden" name="user_id" value="{{$invitation->user['id']}}" />
                            <input type="hidden" name="invitation_id" value="{{$invitation->id}}" />
                                <button type="submit" class="btn_1">Submit review</button>
                                </form>
                        </div>
                    @endisset
                    @else
                    <div class="col-lg-8 ">
                        <div class="box_general write_review">
                                <form method="post" action="{{route('review.store')}}" enctype="multipart/form-data">
                                    @csrf
                                <h1>{{__("Write a review of ")}}{{$data[0]}}</h1>
                                <div class="rating_submit">
                                    <div class="form-group">
                                        <label class="d-block">Overall rating</label>
                                        <span class="rating">


                                        <input type="radio" class="rating-input" id="5_star" required name="rating_score" value="5"><label for="5_star" class="rating-star"></label>
                                        <input type="radio" class="rating-input" id="4_star" required name="rating_score" value="4"><label for="4_star" class="rating-star"></label>
                                        <input type="radio" class="rating-input" id="3_star" required name="rating_score" value="3"><label for="3_star" class="rating-star"></label>
                                        <input type="radio" class="" style="opacity:0" required name="rating_score" >
                                        <input type="radio" class="rating-input" id="2_star" required name="rating_score" value="2"><label for="2_star" class="rating-star"></label>
                                        <input type="radio" class="rating-input" id="1_star" required name="rating_score" value="1"><label for="1_star" class="rating-star"></label>
                                    </span>
                                    </div>
                                </div>
                                <!-- /rating_submit -->
                                <div class="form-group">
                                    <label>Title of your review</label>
                                    <input class="form-control" type="text" required name="title" placeholder="If you could say it in one sentence, what would you say?">
                                </div>
                                <div class="form-group">
                                    <label>Your review</label>
                                    <textarea class="form-control" name="review" required style="height: 180px;" placeholder="Write your review to help others learn about this online business"></textarea>
                                </div>



                                    <p class="text-muted">{{__('Your information is important to prevent double rating')}}</p>

                                <div class="form-group">
                                    <label>Your Email</label>
                                <input class="form-control" value=""  type="email" name="email" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>Your Full Name</label>
                                    <input class="form-control" type="text" name="fullname" placeholder="">
                                </div>
                                {{-- <div class="form-group">
                                    <label>Add your photo (optional)</label>
                                    <div class="fileupload">
                                        <input type="file" name="picture" accept="image/*"></div>
                                </div> --}}
                                {{-- <div class="form-group">
                                    <div class="checkboxes float-left add_bottom_15 add_top_15">
                                        <label class="container_check">Eos tollit ancillae ea, lorem consulatu qui ne, eu eros eirmod scaevola sea. Et nec tantas accusamus salutatus, sit commodo veritus te, erat legere fabulas has ut. Rebum laudem cum ea, ius essent fuisset ut. Viderer petentium cu his.
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div> --}}
                                <input type="hidden" name="user_id" value="{{$data[1]}}" />
                                    <button type="submit" class="btn_1">Submit review</button>
                                    </form>
                            </div>
                    @endisset


                </div>
                <!-- /col -->
                {{-- <div class="col-lg-4">
                    <div class="latest_review">
                        <h4>Recent reviews<br>for Good Electronics</h4>
                        <div class="review_listing">
                            <div class="clearfix add_bottom_10">
                                <figure><img src="img/avatar3.jpg" alt=""></figure>
                                <span class="rating"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star empty"></i><em>4.50/5.00</em></span>
                                <small>Shops</small>
                            </div>
                            <h3><strong>Jhon Doe</strong></h3>
                            <h4>"Avesome Experience"</h4>
                            <p>Et nec tantas accusamus salutatus, sit commodo veritus te</p>
                            <ul class="clearfix">
                                <li><small>26.08.2018</small></li>
                                <li><a href="reviews-page.html" class="btn_1 small">Read review</a></li>
                            </ul>
                        </div>
                        <!-- /review_listing -->
                        <div class="review_listing">
                            <div class="clearfix add_bottom_10">
                                <figure><img src="img/avatar4.jpg" alt=""></figure>
                                <span class="rating"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star empty"></i><em>4.50/5.00</em></span>
                                <small>Shops</small>
                            </div>
                            <h3><strong>Jhon Doe</strong></h3>
                            <h4>"Avesome Experience"</h4>
                            <p>Et nec tantas accusamus salutatus, sit commodo veritus te</p>
                            <ul class="clearfix">
                                <li><small>26.08.2018</small></li>
                                <li><a href="reviews-page.html" class="btn_1 small">Read review</a></li>
                            </ul>
                        </div>
                        <!-- /review_listing -->
                    </div>
                    <!-- /latest_review -->

                </div> --}}
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </main>

@endsection
