@extends('layouts.master')


@section('content')
    <main>
        <div id="results">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-lg-3 col-md-4 col-10">
                    <h1><strong>{{count($reviews)}}</strong> {{__('Results')}}</h1>
                    </div>
                    <div class="col-xl-5 col-md-6 col-2">
                        <a href="#0" class="search_mob btn_search_mobile"></a> <!-- /open search panel -->
                        <form method="get" action="{{route('review.search')}}">
                                @csrf
                            <div class="row no-gutters custom-search-input-2 inner">

                                <div class="col-lg-11">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="realtor" placeholder="Search reviews for a company">
                                        {{-- <i class="icon_search"></i> --}}
                                    </div>
                                </div>
                                {{-- <div class="col-lg-4">
                                    <select class="wide">
                                        <option>All Categories</option>
                                        <option>Shops</option>
                                        <option>Hotels</option>
                                        <option>Restaurants</option>
                                        <option>Bars</option>
                                        <option>Events</option>
                                        <option>Fitness</option>
                                    </select>
                                </div> --}}
                                <div class="col-xl-1 col-lg-1">
                                    <input type="submit" value="Search">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /row -->
                {{-- <div class="search_mob_wp">
                    <div class="custom-search-input-2">
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Search reviews...">
                            <i class="icon_search"></i>
                        </div>
                        <select class="wide">
                            <option>All Categories</option>
                            <option>Shops</option>
                            <option>Hotels</option>
                            <option>Restaurants</option>
                            <option>Bars</option>
                            <option>Events</option>
                            <option>Fitness</option>
                        </select>
                        <input type="submit" value="Search">
                    </div>
                </div> --}}
                <!-- /search_mobile -->
            </div>
            <!-- /container -->
        </div>
        <!-- /results -->

        {{-- <div class="filters_listing sticky_horizontal">
            <div class="container">
                <ul class="clearfix">
                    <li>
                        <div class="switch-field">
                            <input type="radio" id="all" name="listing_filter" value="all" checked data-filter="*" class="selected">
                            <label for="all">All</label>
                            <input type="radio" id="latest" name="listing_filter" value="latest" data-filter=".latest">
                            <label for="latest">Latest</label>
                            <input type="radio" id="oldest" name="listing_filter" value="oldest" data-filter=".oldest">
                            <label for="oldest">Oldest</label>
                        </div>
                    </li>
                    <li><a class="btn_filt" data-toggle="collapse" href="#filters" aria-expanded="false" aria-controls="filters" data-text-swap="Less filters" data-text-original="More filters">More filters</a></li>
                </ul>
            </div>
            <!-- /container -->
        </div> --}}
        <!-- /filters -->

        {{-- <div class="collapse" id="filters">
            <div class="container margin_30_5">
                <div class="row">
                    <div class="col-md-4">
                        <h6>Rating</h6>
                        <ul>
                            <li>
                                <label class="container_check">Superb 9+ <small>67</small>
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container_check">Very Good 8+ <small>89</small>
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container_check">Good 7+ <small>45</small>
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container_check">Pleasant 6+ <small>78</small>
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h6>Categories</h6>
                        <ul>
                            <li>
                                <label class="container_check">Restaurants <small>12</small>
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container_check">Clothes <small>11</small>
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container_check">Bars <small>23</small>
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container_check">Events <small>56</small>
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <div class="add_bottom_30">
                            <h6>Distance</h6>
                            <div class="distance"> Radius around selected destination <span></span> km</div>
                            <input type="range" min="10" max="100" step="10" value="30" data-orientation="horizontal">
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
        </div> --}}
        <!-- /Filters -->

        <div class="container margin_60_35">

            <div class="isotope-wrapper">
                <div class="row">
                    @isset($reviews)
                        @if(count($reviews))
                            @foreach($reviews as $review)
                                <div class="col-xl-4 col-lg-6 col-md-6 isotope-item latest">
                                    <div class="review_listing">
                                        <div class="clearfix add_bottom_15">
                                            <figure><img src="{{asset('img/avatar1.jpg')}}" alt=""></figure>
                                            @if($review)
                                            <span class="rating">
                                                @for($i = 0; $i < $review['rating_score'];  $i++)
                                                    <i class="icon_star"></i>
                                                @endfor
                                                @for($i = 0; $i < 5 - $review['rating_score'];  $i++)
                                                    <i class="icon_star empty"></i>
                                                @endfor
                                                <em>{{number_format($review['rating_score'], 2)}}/5.00</em>
                                            </span>
                                            @endif




                                            {{-- <small>Shops</small> --}}
                                        </div>
                                        <h3><strong>
                                            @if(!empty($review['contact']))
                                                {{ucwords($review['contact']['fullname'])}}
                                            @else
                                            {{ucwords($review['fullname'])}}
                                            @endif
                                        </strong> reviewed <a href="{{route('user.view',$review['business_name_slug'] )}}"> {{ucwords($review['business_name'])}}</a></h3>
                                        <h4>"{{$review['title']}}"</h4>
                                    <p>{{$review['review']}}</p>
                                        <ul class="clearfix">
                                            <li><small>{{__('Published:')}} {{when($review['created_at'])}}</small></li>
                                            {{-- <li><a href="reviews-page.html" class="btn_1 small">Read review</a></li> --}}
                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                        @else
                        <div class="company_listing isotope-item high">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="company_info alert text-center">
                                    <img style="width:100px;display:block;margin:auto" src='{{asset('img/broker.png')}}' class="img-fluid" />
                                        <h3><strong>Oops!</strong> No reviews could  be found.</h3>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endisset

                    <!-- /review_listing grid -->


                </div>
                <!-- /row -->
            </div>
            <!-- /isotope-wrapper -->

            {{-- <p class="text-center"><a href="#0" class="btn_1 rounded add_top_15">Load more</a></p> --}}

        </div>
        <!-- /container -->

    </main>
    <!--/main-->
@endsection
