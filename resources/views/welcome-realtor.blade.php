@extends('layouts.master')
@push('styles')


@endpush

@section('content')

    <main>
        <section  class="hero_single version_2">
            <div class="wrapper">
                <div class="container">
                        <div class="row justify-content-center pt-lg-5">
                                <div class="col-xl-5 col-lg-6">
                                        <h3>{{@$index['realtor-banner-title-lg']}}</h3>
                                        <p>{{@$index['realtor-banner-title-sm']}}</p>
                                        <p><a href="#sign-in-dialog" id="sign-in4" class="btn_1 add_top_10 wow bounceIn" title="Sign In">Join {{config('app.name')}} Now!</a></p>

                                </div>
                                <div class="col-xl-5 col-lg-6 text-right d-none d-lg-block">
                                    <img src="img/graphic_home_2.svg" alt="" class="img-fluid">
                                </div>
                        </div>


                </div>
            </div>
        </section>
        <!-- /hero_single -->

        <div class="bg_color_1">
            <div class="container margin_60_35">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="box_feat">
                            <i class="pe-7s-speaker"></i>
                            <h3><strong>30</strong> thousand<em>reviews seen every month</em></h3>
                            <p>Over 30 thousand review impressions every month</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="box_feat">
                            <i class="pe-7s-flag"></i>
                            <h3><strong>5</strong> thousand<em>real reviews per month</em></h3>
                            <p>Over 5 thousand reviews posted every month</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="box_feat">
                            <i class="pe-7s-rocket"></i>
                            <h3><strong>1</strong> thousand<em>have a great return</em></h3>
                            <p>Over 1 thousand companies increase their business</p>
                        </div>
                    </div>
                </div>
                <!-- /row -->
                {{-- <div class="margin_60">
                    <h5 class="text-center add_bottom_30">More than 1000 companies use Realtor!</h5>
                    <div id="brands" class="owl-carousel owl-theme">
                        <div class="item">
                            <a href="#"><img src="img/brands/1_c.jpg" alt=""></a>
                        </div>
                        <!-- /item -->
                        <div class="item">
                            <a href="#"><img src="img/brands/2_c.jpg" alt=""></a>
                        </div>
                        <!-- /item -->
                        <div class="item">
                            <a href="#"><img src="img/brands/3_c.jpg" alt=""></a>
                        </div>
                        <!-- /item -->
                        <div class="item">
                            <a href="#"><img src="img/brands/4_c.jpg" alt=""></a>
                        </div>
                        <!-- /item -->
                        <div class="item">
                            <a href="#"><img src="img/brands/5_c.jpg" alt=""></a>
                        </div>
                        <!-- /item -->
                        <div class="item">
                            <a href="#"><img src="img/brands/6_c.jpg" alt=""></a>
                        </div>
                        <!-- /item -->
                        <div class="item">
                            <a href="#"><img src="img/brands/7_c.jpg" alt=""></a>
                        </div>
                        <!-- /item -->
                        <div class="item">
                            <a href="#"><img src="img/brands/8_c.jpg" alt=""></a>
                        </div>
                        <!-- /item -->
                    </div>
                    <!-- /carousel -->
                </div> --}}
            </div>
            <!-- /container -->
        </div>
        <!-- /bg_color_1 -->

        <div style="background-position:100% -500%" class="call_section">
            <div class="container margin_80_55">
                <div class="row justify-content-center">
                        <div class="col-xl-5 col-lg-6 pt-lg-5">
                                <h3>Let's Help You</h3>
                                <p>{{@$index['site-description']}}</p>
                                <p><a href="#sign-in-dialog" id="sign-in2" class="btn_1 add_top_10 wow bounceIn" title="Sign In">Join {{config('app.name')}} Now!</a></p>
                            </div>
                    <div class="col-xl-5 col-lg-6">
                        <img alt="" class="img-fluid" src="img/graphic_home_1.svg">
                    </div>

                </div>
            </div>
            <!-- /container -->
        </div>


        <div class="feat_blocks">
            <div class="container-fluid h-100">
                <div class="row h-100 justify-content-center align-items-center">
                    <div class="col-md-6 p-0">
                        <div class="block_1"><img src="img/company_info_graphic_1.svg" alt="" class="img-fluid"></div>
                    </div>
                    <div class="col-md-6 p-0">
                        <div class="block_2">
                            <h3>Increase conversions with the power of your customers</h3>
                            <p>{{@$index['pricing-description']}}</p>
                            <a href="{{route('pricing')}}" class="btn_1">View Pricing Plans</a>
                        </div>
                    </div>

                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /feat_blocks -->


        {{-- @isset($features)
        <div class="bg_color_1">
            <div class="container margin_60_35">
                <div class="main_title_2">
                    <h2>Features</h2>
                    <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="box_feat_2">
                            <h3><i class="pe-7s-graph3"></i>Analytics Tools</h3>
                            <p><strong>Mucius doctus constituto pri at.</strong> At vix utinam corpora, ea oblique moderatius usu. Vix id viris consul honestatis, an constituto deterruisset consectetuer pro quo corrumpit euripidis.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box_feat_2">
                            <h3><i class="pe-7s-share"></i>Social Integration</h3>
                            <p><strong>Mucius doctus constituto pri at.</strong> At vix utinam corpora, ea oblique moderatius usu. Vix id viris consul honestatis, an constituto deterruisset consectetuer pro quo corrumpit euripidis.</p>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /container -->
        </div>
        @endisset --}}

        @isset($features)
        <div class="bg_color_1">
            <div class="container margin_60_35">
                <div class="main_title_2">
                    <h2>Features</h2>
                    {{-- <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p> --}}
                </div>
                @if(count($features))
                <div class="row">

                        @foreach($features as $key => $value)

                                <div class="col-md-6">
                                    <div class="box_feat_2">
                                        <h3><i class="pe-7s-graph3"></i>{{$key}}</h3>
                                    <p><strong>{{$value}}</strong></p>
                                    </div>
                                </div>

                                        {{-- <div class="col-md-6">
                                            <div class="box_feat_2">
                                                <h3><i class="pe-7s-graph3"></i>Analytics Tools</h3>
                                                <p><strong>Mucius doctus constituto pri at.</strong> At vix utinam corpora, ea oblique moderatius usu. Vix id viris consul honestatis, an constituto deterruisset consectetuer pro quo corrumpit euripidis.</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="box_feat_2">
                                                <h3><i class="pe-7s-share"></i>Social Integration</h3>
                                                <p><strong>Mucius doctus constituto pri at.</strong> At vix utinam corpora, ea oblique moderatius usu. Vix id viris consul honestatis, an constituto deterruisset consectetuer pro quo corrumpit euripidis.</p>
                                            </div>
                                        </div> --}}


                        @endforeach

                </div>
                @endif

            </div>
            <!-- /container -->
        </div>
        @endisset
        <!-- /bg_color_1 -->

        <div class="call_section_2">
            <div class="wrapper">
                <div class="container">
                    <h3>Get started now with Realtor...improve your business.</h3>
                    <p><a href="#sign-in-dialog" id="sign-in3" class="btn_1 add_top_10 wow bounceIn" title="Sign In">Join {{config('app.name')}} Now!</a></p>
                </div>
            </div>
        </div>
        <!-- /call_section_2 -->

    </main>

@endsection

@push('scripts')


@endpush
