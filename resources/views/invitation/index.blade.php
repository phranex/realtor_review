@extends('layouts.realtor')


@section('content')
    <div class="row">
        <div class="col-12">
            <div class="text-right w-100 m-b-10"><button type="button" id="invite" class="btn btn-primary">Send Requests</button></div>

            <div class="card card-statistics">
                <div class="card-header">
                    <div class="card-heading">
                        <h4 class="card-title">Requests</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                {{-- <th scope="col">Invite profile Link</th> --}}
                                <th scope="col">Email</th>
                                <th scope="col">Contact Info</th>
                                <th scope="col">Response</th>
                            </tr>
                            </thead>
                            <tbody>

                            @isset($invitations['invites'])

                                @if(count($invitations['invites']))

                                    @foreach ($invitations['invites'] as $item)
                                        <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                            {{-- <td><a href="{{route('user.invitation.show',$item['invitation_code'])}}">Link</a></td> --}}
                                            <td>{{$item['email']}}</td>
                                        <td>
                                            @isset($item['contact'])
                                                {{$item['contact']['fullname']}}
                                            @endisset
                                        </td>
                                        <td>
                                            @if($item['status'])
                                                <i class="fa fa-check"></i> Reviewed
                                            @endif
                                            {{-- @isset($item['review'])
                                            <a class="btn btn-xs btn-outline-primary" href="{{route('user.invitation.show',$item['code'])}}">view</a>
                                            @endisset --}}
                                        </td>
                                        </tr>
                                    @endforeach
                                @endif
                            @endisset


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    {{--<h5 class="modal-title">Modal title</h5>--}}
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div>
                    <form method="POST" id='send-request' action="{{route('user.invitation.store')}}">
                            @csrf
                            <div class="alert form-group">
                                <label>Send request to: </label> <small><div class="form-check" style="display: inline">
                                        @if($invitations['num_of_contacts'])<input name='all' class="form-check-input" type="checkbox" id="gridCheck">
                                        <label class="form-check-label" for="gridCheck">
                                            All contacts
                                        </label> @endif
                                    </div></small>
                                <p id='em' style='padding:20px;border-bottom:1px solid grey' contenteditable='true'></p>
                                <small>Enter emails separated only by comma</small>
                                <input id='emails'  type="hidden"  placeholder="Enter emails separated by comma" name="emails" class="form-control">
                            </div>

                    </div>

                    <!-- begin row -->

                            <div class=" alert form-group">
                                <div class="card-header">
                                    <div class="card-heading">
                                        <h4 class="card-title"><p>Construct your invite message</p></h4>
                                    </div>
                                </div>
                                <div class="aler">
                                    <textarea id="editor"  name="message"  class="summernot"></textarea>
                                </div>
                            </div>

                    <!-- end row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Send</button>
                </div>
            </form>
            </div>
        </div>
    </div>






@endsection

@push('scripts')
<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script src="{{asset("js/validate.js")}}"></script>
    <script>
        CKEDITOR.replace( 'editor' );
        $(document).ready(function(){
            $('#invite').click(function () {

                $('#largeModal').modal();

            });


            $('#gridCheck').click(function () {

                if($(this).is(':checked')){
                    $('#emails').hide().prop('required', false);
                }else{

                    $('#emails').show().prop('required', true);
                }
            });

        });


        $('#send-request').submit(function(){

                event.preventDefault();
                var ed = CKEDITOR.instances.editor.getData();
                console.log(ed);
                var emails = $('#em').text();
                // emails = emails.trim();
                emails = emails.split(",");
                console.log(emails);

                if(emails.length){

                    var arr_mail = [];
                    var arr_error = [];
                    for(i = 0; i < emails.length; i++){
                        if(isEmail(emails[i])){
                            arr_mail.push(emails[i]);
                        }else{
                            arr_error.push(emails[i]);
                        }
                    }

                    if(arr_error.length == 0){
                        $('#emails').val(emails);
                        $('#editor').val(ed);
                        if(ed.length <= 1){
                            toastr.error('Enter a message');
                        }else $(this).unbind().submit();
                    }else{
                        if(emails.length == 1){
                            if(emails[0] == ''){
                                toastr.error('Please enter at least an email');
                            }else{
                                var mess = 'The following mails are incorrect. Please correct and resend' + '<br/>';
                            for(i = 0; i < arr_error.length; i++){
                                mess += '- ' + arr_error[i] + '<br/>';
                            }
                            toastr.error(mess);
                            }
                        }else if(arr_error.length){
                            var mess = 'The following mails are incorrect. Please correct and resend' + '<br/>';
                            for(i = 0; i < arr_error.length; i++){
                                mess += '- ' + arr_error[i] + '<br/>';
                            }
                            toastr.error(mess);
                        }


                    }

                }else{
                    toastr.error('Please enter at least an email');
                }


        });

    </script>
@endpush

