@extends('layouts.master')


@section('content')

<main style="transform: none;">

    <!-- /hero_single -->

     <div class="container margin_60_35" style="transform: none;">
        <div class="row" style="transform: none;">

            <!--/aside -->

            <div style="margin-top:30px" class="col-lg-9 mx-auto" id="faq">
                    <div class="card">
                            <div class="card-header text-center" role="tab">
                                <h5 class="mb-0 ">Terms and Conditions</h5>
                            </div>

                            <div>
                                <div class="card-body">
                                    {!! $terms !!}
                                </div>
                            </div>
                    </div>

            </div>
            <!-- /col -->
        </div>
        <!-- /row -->
    </div>
    <!--/container-->
</main>
@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $('.toggleChevron').click(function(){
                $(this).find('i.indicator').toggleClass('ti-plus ti-minus');
            });
        });
    </script>

@endpush
