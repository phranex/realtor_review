<aside class="app-navbar">
    <!-- begin sidebar-nav -->
    <div class="sidebar-nav scrollbar scroll_light">
        <ul class="metismenu " id="sidebarNav">
            <li class="nav-static-title">Personal</li>
            <li class="@if(request()->route()->getName() ==  'user.dashboard') active @endif">
                <a class="" href="{{route('user.dashboard')}}" aria-expanded="false">
                    <i class="nav-icon ti ti-rocket"></i>
                    <span class="nav-title">Dashboard</span>

                </a>

            </li>
            <li class="@if(request()->route()->getName() ==  'user.contacts') active @endif"><a href="{{route('user.contacts')}}" aria-expanded="false"><i class="nav-icon ti ti-comment"></i><span class="nav-title">Contacts</span></a> </li>
            <li class="@if(request()->route()->getName() ==  'user.invitations') active @endif"><a class="has-arrow" href="{{route('user.invitations')}}" aria-expanded="false"><i class="nav-icon ti ti-calendar"></i><span class="nav-title">Request Reviews</span></a>

            </li>
            <li class="@if(request()->route()->getName() ==  'user.reviews') active @endif"><a href="{{route('user.view',str_replace(' ','_',session('user.setting.business_name')))}}" aria-expanded="false"><i class="nav-icon ti ti-email"></i><span class="nav-title">Reviews</span></a> </li>
            <li class="@if(request()->route()->getName() ==  'user.integrations') active @endif">
                <a class="has-arrow" href="{{route('user.integrations')}}" aria-expanded="false"><i class="nav-icon ti ti-bag"></i> <span class="nav-title">Integrations</span></a>

            </li>
            <li class="@if(request()->route()->getName() ==  'user.dashboard-profile') active @endif">
                <a class="has-arrow" href="{{route('user.profile')}}" aria-expanded="false"><i class="nav-icon ti ti-info"></i><span class="nav-title">Profile</span> </a>

            </li>
            {{--<li class="nav-static-title">Widgets, Tables & Layouts</li>--}}

            {{--<li class="@if(request()->route()->getName() ==  'user.reviews') active @endif">--}}
                {{--<a class="has-arrow" href="{{route('user.reviews')}}" aria-expanded="false"><i class="nav-icon ti ti-layout-column3-alt"></i><span class="nav-title">Settings</span></a>--}}

            {{--</li>--}}







        </ul>
        <div class="text-center">
        <a class="has-arro btn btn-sm btn-primary" href="{{route('packages')}}" aria-expanded="false"><span class="nav-title">Upgrade</span></a>

        </div>

    </div>
    <!-- end sidebar-nav -->
</aside>
