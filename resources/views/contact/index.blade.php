@extends('layouts.realtor')
@push('styles')
    <style>
        .w-50{
            width: 50% !important;
        }

        .form-filter small{
            display: block;
            margin-top: 10px;
        }

        .form-filter{
           
        }

        .form-filter select {
            width: 50%;
        }

        @media (max-width: 500px){
            .form-filter select {
            width: 100% !important;
           }

           input.search{
               margin-top: 10px ;
           }
        }


    </style>
@endpush


@section('content')
    <!-- begin row -->
    <div class="row">
        <div class="col-md-12 m-b-30">
            <!-- begin page title -->
            <div class="d-block d-sm-flex flex-nowrap align-items-center">
                <div class="page-title mb-2 mb-sm-0">
                    <h1>Contacts</h1>
                </div>
                {{-- <div class="ml-auto d-flex align-items-center">
                    <nav>
                        <ol class="breadcrumb p-0 m-b-0">
                            <li class="breadcrumb-item">
                                <a href="index.html"><i class="ti ti-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                Tables
                            </li>
                            <li class="breadcrumb-item active text-primary" aria-current="page">Contact</li>
                        </ol>
                    </nav>
                </div> --}}
            </div>
            <!-- end page title -->
        </div>
    </div>
    <!-- end row -->
    <!-- begin row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-statistics">
                <div class="card-body">
                        <div class="text-righ w-100 row" style="margin-bottom:10px">
                                <div class="col-lg-8 col-12">
                                    <button id='add-contact' class="btn  btn-xs btn-outline-primary"><i class="fa fa-plus-square"></i> Add Contact</button>
                                    
                                    <form id="add-contact-form" method="POST" action="{{route('user.contact.store')}}" style="display:none">
                                            @csrf<div class="form-row">
                                                    <div class="form-group col-md-3">
                                                        <label for="name1">Full Name</label>
                                                        <input type="text" required class="form-control" id="name" required name="fullname" value="">
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="title1">Email</label>
                                                    <input type="email" required class="form-control" required name="email"  value="">
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="phone1">Phone Number</label>
                                                        <input type="number" class="form-control" required name="phone_number" id="phone" value="">
                                                    </div>
                                                    <input type="hidden" name="contact_id" />
                                                    <div class="form-group text-right  col-md-2">
                                                        <label class="w-100" > &nbsp;</label>
                                                        <input type="submit" id='contact-submit' class="btn  btn-md btn-primary"  value="Add">
                                                    </div>
                                            </div>
                                    </form>
                                    <form class="form-filter" action='#' style="display:inline">
                                       <small style="font-size:xx-small">Show</small> <select class="form-control" style="display:inline;height:30px" name="filter">
                                            <option @if(request('filter') == 'all') selected @endif value="all">All Contacts</option>
                                            <option @if(request('filter') == 1) selected @endif value="1">Request Sent</option>
                                            <option @if(request('filter') == 2) selected @endif value="2">Request Not Sent</option>
                                            <option @if(request('filter') == 3) selected @endif value="3">Responded</option>
                                            <option @if(request('filter') == 4) selected @endif value="4">Not Responsed</option>
                                        </select>
                                    </form>
                                    <button style="display:none" type="button" id="invite" class="btn btn-success send-request-btn btn-xs">Send Requests</button>

                                </div>

                                {{-- <button type="button" id="upload" class="btn btn-default">Import from Excel</button> --}}
                                <form class="text-right  col-lg-4 col-12  text-right" action="{{route('user.contacts')}}" method="get">
                                    @csrf

                                <input type="text" value='{{request('query')}}' style="height:30px" name="query" class="form-control search w-30 d-inline" />
                                        <input type="submit" class="btn btn-xs btn-primary" value="Search" />

                                </form>
                            </div>
                       
                    <div class="export-table-wrapper table-responsive">


                        <table id="" class="table table-bordered">
                            <thead class="thead-light">
                            <tr>
                                <th>S/N</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Action</th>



                            </tr>
                            </thead>
                            <tbody>
                                @isset($contacts)
                                    @if(count($contacts))
                                        @foreach($contacts as $contact)
                                            <tr>
                                                <td>
                                                        <div class="form-check">
                                                        <input class="form-check-input" value='{{$contact['id']}}' name="contact-check" type="checkbox" value="" id="defaultCheck{{$loop->iteration}}">
                                                                <label class="form-check-label" for="defaultCheck{{$loop->iteration}}">
                                                                  Select
                                                                </label>
                                                              </div>
                                                </td>
                                            <td>{{$contact['fullname']}}</td>
                                            <td>{{$contact['email']}}</td>
                                                <td>{{$contact['phone_number']}}</td>
                                                <td>
                                                <button data-item="{{ json_encode( $contact, true) }}" class="btn btn-xs edit-contact btn-outline-info"><i class='fa fa-pencil'></i></button>
                                                <button data-href = '{{base64_encode(route('user.contact.delete',$contact['id']))}}'  class="btn btn-xs delete btn-outline-danger"><i class='fa fa-trash-o'></i></button>

                                            </td>

                                            </tr>
                                        @endforeach
                                    @else
                                            <tr>
                                                <td colspan="5"><div class="text-center alert">No contact found</div></td>
                                            </tr>
                                    @endif
                                @endisset


                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->


    <!-- Large Modal -->


    <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Construct your Message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">


                        <form id="request-form" method="POST" id='send-request' action="{{route('user.invitation.store')}}">
                                @csrf



                                <!-- begin row -->
                                        <div class="form-group row">
                                            @if(isset($templates))
                                                @if(count($templates))

                                                    <div id='existing-template' class="col-5">
                                                            <label>Select a Template</label>
                                                            <select name='template' class="form-control">

                                                                    <option value=''>Select</option>
                                                                    @foreach($templates as $template)
                                                            <option data-value="{{json_encode($template,true)}}" value="{{$template['id']}}">{{$template['name']}}</option>
                                                                    @endforeach
                                                            </select>
                                                    </div>
                                                @endif
                                            @endif

                                            <div class="col-3 " style="positive:relativ">

                                                <span style="">` <button type="button"  class="btn btn-link create-new-template">Create New Template</button></span>
                                            </div>
                                        </div>

                                        <div id='create-new' style="display:none" class="form-group">

                                                <div style='display:none' id='templateName' class="form-group">
                                                        <label>Template Name</label>
                                                    <input type="text"  name="template_name" placeholder="Give this template a name" class="form-control" />
                                                </div>
                                                <div id="message_box" class="aler">
                                                        <label>Construct your invite message</label>
                                                        <textarea id="editor"   name="message"  class="summernot"></textarea>
                                                </div>
                                                <input type="hidden" name='emails' id='hidden' />
                                                <input type="hidden" name="new" value="1" />
                                        </div>



                                <!-- end row -->

                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </div>
@endsection
@push('scripts')
<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script src="{{asset("js/validate.js")}}"></script>
    <script>
         CKEDITOR.replace( 'editor' );

         $('#request-form').submit(function(){
             event.preventDefault();
             var message = CKEDITOR.instances.editor.getData();
             if(!message.length){
                toastr.error('Please select a template or create a new template');
             }else{
                 $(this).unbind().submit()
             }

         });
         $('[name=template]').change(function(){
             if($(this).val().length){
                $('#create-new').show();
                $('#templateName').hide();
                $('#message_box').show();
                var val = $(this).children('option:selected').attr('data-value');
                val = JSON.parse(val);
                CKEDITOR.instances.editor.setData(val.message);

             }else{
                CKEDITOR.instances.editor.setData('');
                $('#message_box').hide();
             }
         });
        $(document).ready(function(){
            $('#upload').click(function () {
                $('#largeModal').modal();
                $('#create-new').hide();
                CKEDITOR.instances.editor.setData('');
                $('#templateName').val('');

            });
            $('.create-new-template').click(function(){



                if($(this).text().toLowerCase() == 'create new template'){
                    $('#create-new').show();
                    $('#templateName').show();
                    $('#message_box').show();
                    @if(@$templates)
                    $(this).text('Use existing  template');
                    @else
                    $(this).text('Cancel');
                    @endif
                    $('#existing-template').hide();
                    CKEDITOR.instances.editor.setData('');

                }else{
                    $('#create-new').hide();
                    $('#templateName').hide();
                    $(this).text('Create new Template');
                    $('#existing-template').show();
                    if($('[name=template]').val().length){
                        console.log($('[name=template]').val());
                        $('#create-new').show();
                        $('#templateName').hide();
                        $('#message_box').show();
                        var val = $('[name=template]').children('option:selected').attr('data-value');
                        val = JSON.parse(val);

                        CKEDITOR.instances.editor.setData(val.message);

                    }else{
                        $('#message_box').hide();
                    }
                }

            });
            $('#invite').click(function () {

                $('#largeModal').modal();
                $('#largeModal').modal();
                $('#create-new').hide();
                CKEDITOR.instances.editor.setData('');
                $('#templateName').val('');
                var contacts = [];
                $('[name=contact-check]').each(function(){
                    if($(this).is(':checked')){
                    contacts.push($(this).val());
                    }
                });
                console.log(contacts);
                $('#hidden').val(contacts);

            });

            $('[name=query]').focusin(function(){
                $(this).css({

                    'border': '1px solid blue',
                    'transition': '.5s'
                }).addClass('w-50');
            }).focusout(function(){
                $(this).css({

                    'border': '1px solid #dee2e6',
                'transition': '.5s'
                }).removeClass('w-50');
            });
            $('.delete').click(function(){
                var answer = confirm('Are you sure? Click ok to continue');

                if(answer){
                    window.location.href = atob($(this).attr('data-href'));
                }
            });
            $('[name=filter]').change(function(){
                var value = $(this).val();
                console.log(value);
                if(value == 'all') window.location.href = '{{route('user.contacts')}}';
               else  window.location.href = '{{route('user.contacts')}}?filter='+value;
            });
            $('.download-template').click(function(){
                var url = '{{route('contact.bulk-upload.format')}}';
                $.ajax({
                    url: url,
                    type: 'get',
                    success:function(result){
                        console.log(result);
                    },
                    error:function(e){
                        console.log(e);
                    }
                });
            });
            $('.edit-contact').click(function(){
                var contact = JSON.parse($(this).attr('data-item'))
               $('[name=fullname]').val(contact.fullname);
               $('[name=email]').val(contact.email);
               $('[name=phone_number]').val(contact.phone_number);
               $('[name=contact_id]').val(contact.id);
               $('#add-contact-form').attr('action','{{route('user.contact.update')}}')
               $('#add-contact-form').show().fadeIn();
                $('#add-contact').text('Cancel');
                $('#add-contact').addClass('btn-danger').removeClass('btn-outline-primary');
                $('#contact-submit').val('Edit');
            });

            $('#add-contact').click(function(){
                $('#contact-submit').val('Add');
                $('.form-control').val('');
                if($('#add-contact-form').css('display') == 'none'){
                    $('#add-contact-form').show().fadeIn();
                    $(this).text('Cancel');
                    $(this).addClass('btn-danger').removeClass('btn-outline-primary');
                }else{
                    $(this).text('Add Contact');
                    $(this).addClass('btn-outline-primary').removeClass('btn-danger')
                    $('#add-contact-form').hide().fadeOut();
                }

            });

            $('[name=contact-check]').click(function(){
              if($('[name=contact-check]:checked').length){
                $('.send-request-btn').show();
              }else{
                $('.send-request-btn').hide();
              }
            });

            $('.send-re-btn').click(function(){
              var candidates = [];
              $('[name=mail]').each(function(){
                if($(this).is(':checked')){
                  candidates.push($(this).val());
                }
              });
              $('.send-mail-btn').attr('type', 'submit');
              $('#hidden').val(candidates);
              $('form#send').submit();

            });

        });

    </script>
@endpush
