<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{config('app.name')}}">
    <meta name="Etoka Francis" content="francis.dretoka@gmail.com">
    <title>{{config('app.name')}} </title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('img/apple-touch-icon-57x57-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{asset('img/apple-touch-icon-72x72-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{asset('img/apple-touch-icon-114x114-precomposed.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{asset('img/apple-touch-icon-144x144-precomposed.png')}}">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/vendors.css')}}" rel="stylesheet">
    <link href="{{asset('css/jquery.toast.css')}}" rel="stylesheet">


    <!-- YOUR CUSTOM CSS -->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    @stack('styles')

</head>

<body>

<div id="page">

    <header class="@if(in_array(request()->route()->getName(), ['review.create','pricing','faq','terms', 'policy','password.request','password.reset','user.view','user.invitation.show', 'review.submitted']))header_in is_fixed @elseif(in_array(request()->route()->getName(), ['review.search', 'review.show-random'])) header_in @else header  menu_fixed @endif ">
        @if(!in_array(request()->route()->getName(), ['landing-page','realtor.landing-page']))
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-12">
                    <div id="logo">
                        <a href="{{route('landing-page')}}">
                            @if(in_array(request()->route()->getName(), ['landing-page','realtor.landing-page']))
                            {{--<img src="{{asset('img/logo.svg')}}" width="140" height="35" alt="" class="logo_normal">--}}
                            <img src="{{asset('img/logo.png')}}" width="140" height="35" alt="" class="logo_normal img-fluid">
                            @endif
                            <img src="{{asset('img/logo.png')}}" width="140" height="35" alt="" class="logo_sticky img-fluid">
                            {{--<img src="{{asset('img/logo_sticky.svg')}}" width="140" height="35" alt="" class="logo_sticky">--}}
                        </a>
                    </div>
                </div>
                <div class="col-lg-9 col-12">
                    <ul id="top_menu">

                        @if(session()->has('user'))
                        <li><a href="{{route('user.profile')}}" class="btn_top company">Hi! {{session('user.first_name')}}</a></li>
                        <li><a href="{{route('user.logout')}}"  class="login" title="Logout">Log Out</a></li>
                        @else
                        {{-- <li><a href="{{route('review.create')}}" class="btn_top">Write a Review</a></li> --}}
                        <li><a href="{{route('realtor.landing-page')}}" class="btn_top company">For Pro</a></li>
                    <li><a href="#sign-in-dialog" id="sign-in" class="login" title="Logout">Logout</a></li>
                        @endif

                    </ul>
                    <!-- /top_menu -->
                    <a href="#menu" class="btn_mobile">
                        <div class="hamburger hamburger--spin" id="hamburger">
                            <div class="hamburger-box">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>
                    </a>
                    <!-- /btn_mobile -->
                    <nav id="menu" class="main-menu ">
                        <ul>
                            <li><span><a href="{{route('landing-page')}}">Home</a></span>

                            </li>
                            <li><span><a href="{{route('review.show-random')}}">Reviews</a></span>

                            </li>
                            <li><span><a href="{{route('pricing')}}">Pricing</a></span></li>


                            @if(session()->has('user'))
                            <li class="d-block d-sm-none"><a href="{{route('user.profile')}}" class="btn_top company">Hi! {{session('user.first_name')}}</a></li>

                            @else
                            {{-- <li class="d-block d-sm-none"><span><a href="{{route('review.create')}}" class="btn_top">Write a review</a></span></li> --}}
                            <li class="d-block d-sm-none"><a href="{{route('realtor.landing-page')}}" class="btn_top company">For Pro</a></li>
                            <li class="d-block d-sm-none"><a href="#sign-in-dialog" id="sign-in" class="login" title="Sign In">Sign In</a></li>
                            @endif

                        </ul>
                    </nav>
                </div>
            </div>
        </div>

            @else
            <div id="logo">
                <a href="{{route('landing-page')}}">
                    @if(in_array(request()->route()->getName(), ['landing-page','realtor.landing-page']))
                        {{--<img src="{{asset('img/logo.svg')}}" width="140" height="35" alt="" class="logo_normal">--}}
                        <img src="{{asset('img/logo.png')}}" width="140" height="35" alt="" class="logo_normal img-fluid">
                    @endif
                    <img src="{{asset('img/logo.png')}}" width="140" height="35" alt="" class="logo_sticky img-fluid">
                    {{--<img src="{{asset('img/logo_sticky.svg')}}" width="140" height="35" alt="" class="logo_sticky">--}}
                </a>
            </div>

            <ul id="top_menu">

                @if(session()->has('user'))
                <li><a href="{{route('user.profile')}}" class="btn_top company">Hi! {{session('user.first_name')}}</a></li>
                @else
                {{-- <li><a href="{{route('review.create')}}" class="btn_top">Write a Review</a></li> --}}
                <li><a href="{{route('realtor.landing-page')}}" class="btn_top company">For Pro</a></li>
                <li><a href="#sign-in-dialog" id="sign-in" class="login" title="Sign In">Sign In</a></li>
                @endif

            </ul>
            <!-- /top_menu -->
            <a href="#menu" class="btn_mobile">
                <div class="hamburger hamburger--spin" id="hamburger">
                    <div class="hamburger-box">
                        <div class="hamburger-inner"></div>
                    </div>
                </div>
            </a>
            <!-- /btn_mobile -->
            <nav id="menu" class="main-menu">
                <ul>
                    <li><span><a href="{{route('landing-page')}}">Home</a></span>

                    </li>
                    <li><span><a href="{{route('review.show-random')}}">Reviews</a></span>

                    </li>
                    <li><span><a href="{{route('pricing')}}">Pricing</a></span></li>

                    {{-- <li class="d-block d-sm-none"><span><a href="{{route('review.create')}}" class="btn_top">Write a review</a></span></li> --}}
                    <li class="d-block d-sm-none"><span><a href="{{route('realtor.landing-page')}}" class="btn_top company">For Pro</a></span></li>

                </ul>
            </nav>
        @endif
    </header>
    <!-- /header -->

    @yield('content')

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <a data-toggle="collapse" data-target="#collapse_ft_1" aria-expanded="false" aria-controls="collapse_ft_1" class="collapse_bt_mobile">
                        <h3>Quick Links</h3>
                        <div class="circle-plus closed">
                            <div class="horizontal"></div>
                            <div class="vertical"></div>
                        </div>
                    </a>
                    <div class="collapse show" id="collapse_ft_1">
                        <ul class="links">
                            {{-- <li><a href="#0">About us</a></li> --}}
                        <li><a href="{{route('faq')}}">Faq</a></li>
                            {{-- <li><a href="#0">Help</a></li> --}}
                        <li><a href="{{route('user.dashboard')}}">My account</a></li>
                            <li><a href="{{route('policy')}}">Privacy Policy</a></li>
                            <li><a href="{{route('terms')}}">Terms and Conditions</a></li>
                        </ul>
                    </div>
                </div>
                {{-- <div class="col-lg-3 col-md-6 col-sm-6">
                    <a data-toggle="collapse" data-target="#collapse_ft_2" aria-expanded="false" aria-controls="collapse_ft_2" class="collapse_bt_mobile">
                        <h3>Categories</h3>
                        <div class="circle-plus closed">
                            <div class="horizontal"></div>
                            <div class="vertical"></div>
                        </div>
                    </a>
                    <div class="collapse show" id="collapse_ft_2">
                        <ul class="links">
                            <li><a href="#0">Shops</a></li>
                            <li><a href="#0">Hotels</a></li>
                            <li><a href="#0">Restaurants</a></li>
                            <li><a href="#0">Bars</a></li>
                            <li><a href="#0">Events</a></li>
                            <li><a href="#0">View all</a></li>
                        </ul>
                    </div>
                </div> --}}
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <a data-toggle="collapse" data-target="#collapse_ft_3" aria-expanded="false" aria-controls="collapse_ft_3" class="collapse_bt_mobile">
                        <h3>Contacts</h3>
                        <div class="circle-plus closed">
                            <div class="horizontal"></div>
                            <div class="vertical"></div>
                        </div>
                    </a>
                    <div class="collapse show" id="collapse_ft_3">
                        <ul class="contacts">
                            <li><i class="ti-home"></i>{{@$contact['address']}}</li>
                            <li><i class="ti-headphone-alt"></i>{{@$contact['phone-number']}}</li>
                            <li><i class="ti-email"></i><a href="#0">{{@$contact['email']}}</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <a data-toggle="collapse" data-target="#collapse_ft_4" aria-expanded="false" aria-controls="collapse_ft_4" class="collapse_bt_mobile">
                        <div class="circle-plus closed">
                            <div class="horizontal"></div>
                            <div class="vertical"></div>
                        </div>
                        <h3>Keep in touch</h3>
                    </a>
                    <div class="collapse show" id="collapse_ft_4">
                        <div id="newsletter">
                            <div id="message-newsletter"></div>
                            <form action="{{route('subscribe')}}" class='form' method='post' >
                                    @csrf
                                <div class="form-group">
                                    <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Your email">
                                    <input type="submit" value="Subscribe" >
                                </div>
                            </form>
                        </div>
                        <div class="follow_us">
                            <h5>Follow Us</h5>
                            <ul>
                                <li><a href="#0"><i class="ti-facebook"></i></a></li>
                                <li><a href="#0"><i class="ti-twitter-alt"></i></a></li>
                                <li><a href="#0"><i class="ti-google"></i></a></li>
                                <li><a href="#0"><i class="ti-pinterest"></i></a></li>
                                <li><a href="#0"><i class="ti-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /row-->
            <hr>
            <div class="row">
                <div class="col-lg-6">
                    <ul id="footer-selector">
                        <li>
                            <div class="styled-select" id="lang-selector">
                                <select>
                                    <option value="English" selected>English</option>
                                    {{-- <option value="French">French</option>
                                    <option value="Spanish">Spanish</option>
                                    <option value="Russian">Russian</option> --}}
                                </select>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <ul id="additional_links">
                    <li><a href="{{route('terms')}}">Terms and conditions</a></li>
                    <li><a href="{{route('policy')}}">Privacy</a></li>
                        <li><span>© 2018 {{config('app.name')}}</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!--/footer-->
</div>
<!-- page -->

<!-- Sign In Popup -->
<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">
    <div class="small-dialog-header">
        <h3 id='auth-label'>Login</h3>
    </div>
    {{-- login --}}
    <div id='login-modal'>
        <form action="{{route('user.login')}}" method="post">
                @csrf
                    <div class="sign-in-wrapper">
                        <a href="{{route('socialLogin','facebook')}}" class="social_bt facebook">Login with Facebook</a>
                        <a href="{{route('socialLogin','google')}}" class="social_bt google">Login with Google</a>
                        <div class="divider"><span>Or</span></div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email">
                            <i class="icon_mail_alt"></i>

                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" value="">
                            <i class="icon_lock_alt"></i>
                        </div>
                        <div class="clearfix add_bottom_15">
                            <div class="checkboxes float-left">
                                <label class="container_check">Remember me
                                    <input name="remember" type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="float-right mt-1"><a id="forgot" href="{{route('password.request') }}">Forgot Password?</a></div>
                        </div>
                        <div class="text-center"><input type="submit" value="Log In" class="btn_1 full-width"></div>
                        <div class="text-center">
                            Don’t have an account? <button id="sign-up-modal" data-label='Register' type="button" class="btn btn-link">Sign up</button>
                        </div>

                    </div>
                </form>
    </div>
    {{-- register --}}
    <div id='register-modal' style="display:none">
            <form action="{{route('user.register')}}" method="POST">
                        @csrf
                        <div class="sign-in-wrapper">

                            <div class="form-group">
                                <label>First Name</label>
                            <input type="text" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" value="{{old('first_name')}}" name="first_name" required>
                                <i class="icon_profile"></i>
                                @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                                 @endif
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" value="{{old('last_name')}}" name="last_name" required>
                                <i class="icon_profile"></i>
                                @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                                 @endif
                            </div>
                            <div class="form-group">
                                <label>Business Name</label>
                            <input type="text" class="form-control {{ $errors->has('business_name') ? ' is-invalid' : '' }}" value="{{old('business_name')}}" name="business_name" required>
                                <i class="icon_briefcase"></i>
                                @if ($errors->has('business_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('business_name') }}</strong>
                                </span>
                                 @endif
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{old('email')}}" name="email" required>
                                <i class="icon_mail_alt"></i>
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                 @endif
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password" required>
                                <i class="icon_lock_alt"></i>

                            </div>
                            <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"" name="password_confirmation" id="password" required>
                                    <i class="icon_lock_alt"></i>
                                </div>

                            <div class="text-center"><input type="submit" value="Register" class="btn_1 full-width"></div>
                            <div class="text-center">
                                    Have an account? <button id="sign-in-modal" data-label='Login' type="button" class="btn btn-link">Login</button>
                            </div>

                        </div>
                    </form>
        </div>


    <!--form -->
</div>
<!-- /Sign In Popup -->

<div id="toTop"></div><!-- Back to top button -->

<!-- COMMON SCRIPTS -->
<script src="{{asset('js/common_scripts.js')}}"></script>
<script src="{{asset('js/functions.js')}}"></script>
<script src="{{asset('js/jquery.toast.js')}}"></script>
<script src="{{asset('js/notifications.js')}}"></script>
<script src="{{asset('assets/validate.js')}}"></script>
<script>
    $(document).ready(function(){
        $('#sign-up-modal').click(function(){
            var label = $(this).attr('data-label');
            $('#auth-label').text(label);
            $('#login-modal').hide();
            $('#register-modal').show().fadeIn();
        });

        $('#sign-in-modal').click(function(){
            var label = $(this).attr('data-label');
            $('#auth-label').text(label);
            $('#login-modal').show().fadeIn();
            $('#register-modal').hide().fadeOut();
        });
    });
</script>

<script>

$(document).ready(function(){
    notif();
    @if(session('register'))
        $('#sign-in').trigger('click');
        $('#auth-label').text('Register');
        $('#login-modal').hide();
        $('#register-modal').show().fadeIn();
        @php session()->forget('register') @endphp
    @elseif(session('login'))
        $('#sign-in').trigger('click');
        $('#auth-label').text('Login');
        $('#register-modal').hide();
        $('#login-modal').show().fadeIn();
        @php session()->forget('login') @endphp
    @endif
});


</script>
    @include('includes.messages')
@stack('scripts')
</body>
</html>

