@extends('layouts.realtor')


@section('content')
    <div class="row">
        <div class="col-12 mb-30">
            <div class="card card-statistics">
                <div class="card-header">
                    <h4 class="card-title">Third Party Integrations</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        @isset($integrations)
                            @if(count($integrations))
                                @foreach ($integrations as $item)
                                <div class="col-sm-4">
                                        <div class="card h-100">
                                            <div class="card-body">
                                                <h4 class="card-title">{{$item['name']}} Reviews </h4>
                                                {{-- <p class="card-text"> Have a google business account, integrate your google reviews into your account.</p> --}}
                                                <p class="card-text"> {{$item['title']}}.</p>
                                                <a href="{{route('user.integration.store',[$item['id'],$item['name']])}}" class="btn btn-primary mt-2">Click to Integrate</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            @endif
                        @endisset

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 mb-30">
            <div class="card card-statistics">
                <div class="card-header">
                    <h4 class="card-title">Integrate your reviews into your website</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <p>Copy the following code and paste on any part of your website you want your reviews to show</p>
                            <textarea style="height:100px" class="form-control" disabled>
                                <div id="review-db-box" ></div>
                                <script data-author="{{$identifier}}" src="{{env('URL')}}js/review-integrator.js"></script>
                            </textarea>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
            <div class="col-lg-12">
                <div class="card card-statistics">
                    <div class="card-body">
                            <div class="text-left w-100" style="margin-bottom:10px">
                                  Third Party Integrations
                                </div>

                        <div class="export-table-wrapper table-responsive">


                            <table id="" class="table table-bordered">
                                <thead class="thead-light">
                                <tr>
                                    <th>S/N</th>
                                    <th>Third Party</th>
                                    <th>Profile</th>
                                    <th>Action</th>



                                </tr>
                                </thead>
                                <tbody>
                                    @isset($user_integrations)
                                        @if(count($user_integrations))
                                            @foreach($user_integrations as $item)
                                                <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td><i class="fa fa-{{strtolower($item['third_party_integration'])}}"></i> {{$item['third_party_integration']}}</td>
                                                <td>{{$item['external_name']}}</td>

                                                <td>

                                                <button data-href = '{{base64_encode(route('user.contact.delete',$item['id']))}}'  class="btn btn-xs delete btn-outline-danger"><i class='fa fa-trash-o'></i></button>
                                                </td>

                                                </tr>
                                            @endforeach
                                        @endif
                                    @endisset


                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>



@endsection
