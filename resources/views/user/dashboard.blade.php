@extends('layouts.realtor')
@push('styles')
    <style>
    .heading {
  font-size: 25px;
  margin-right: 25px;
}

.fa {
  font-size: 25px;
}

.checked {
  color: orange;
}

/* Three column layout */
.side {
  float: left;
  width: 15%;
  margin-top: 10px;
}

.middle {
  float: left;
  width: 70%;
  margin-top: 10px;
}

/* Place text to the right */
.right {
  text-align: right;
}

/* Clear floats after the columns */
.r-row:after {
  content: "";
  display: table;
  clear: both;
}

/* The bar container */
.bar-container {
  width: 100%;
  background-color: #f1f1f1;
  text-align: center;
  color: white;
}

/* Individual bars */
.bar-5 {width: 60%; height: 18px; background-color: #4CAF50;}
.bar-4 {width: 30%; height: 18px; background-color: #2196F3;}
.bar-3 {width: 10%; height: 18px; background-color: #00bcd4;}
.bar-2 {width: 4%; height: 18px; background-color: #ff9800;}
.bar-1 {width: 15%; height: 18px; background-color: #f44336;}

/* Responsive layout - make the columns stack on top of each other instead of next to each other */
@media (max-width: 400px) {
  .side, .middle {
    width: 100%;
  }
  /* Hide the right column on small screens */
  .right {
    display: none;
  }
}

    </style>
@endpush


@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="car card-statistics">
            <div class="row">
                <div class="col-xl-6 col-xxl-4 m-b-30">
                    <div class="card card-statistics h-100 mb-0">
                        <div class="card-header">
                            <h4 class="card-title">User Rating</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                    <div class="col-12">
                                    @for($i = 0; $i < $ratings['avg_rating']; $i++)

                                        <span class="fa fa-star checked"></span>
                                    @endfor
                                    @for($i = 0; $i < 5 - $ratings['avg_rating']; $i++)

                                    <span class="fa fa-star"></span>
                                    @endfor


                                    <p>{{number_format(@$ratings['avg_rating'],1)}} average based on {{@$ratings['total']}} reviews.</p>
                                    <hr style="border:3px solid #f1f1f1">

                                    <div class="r-row">
                                        @php
                                        $arr = array_column((array) $ratings['rating_scores_count'], 'rating_score');
                                        $width = 0;

                                        @endphp
                                        @for($i = 5; $i > 0; $i--)

                                        @if($index = array_search($i, $arr) !== false)
                                            @php
                                                $rate = $ratings['rating_scores_count'][$index]['count'];
                                                $tot = $ratings['total'];
                                                $width = $rate / $tot * 100;
                                                $width = $width.'%';
                                            @endphp
                                        <div class="side">
                                            <div>{{$i}} star</div>
                                        </div>
                                        <div class="middle">
                                            <div class="bar-container">
                                            <div style='width:{{$width}}' class="bar-{{$i}}"></div>
                                            </div>
                                        </div>
                                        <div class="side right">
                                        <div>{{$ratings['rating_scores_count'][$index]['count']}}</div>
                                        </div>
                                        @else
                                        <div class="side">
                                                <div>{{$i}} star</div>
                                            </div>
                                            <div class="middle">
                                                <div class="bar-container">
                                                <div style='width:0px' class="bar-{{$i}} "></div>
                                                </div>
                                            </div>
                                            <div class="side right">
                                            <div>0</div>
                                        </div>

                                        @endif

                                        @endfor

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xxl-3 m-b-30">
                    <div class="card card-statistics h-100 mb-0 widget-income-list">
                        <div class="card-body d-flex align-itemes-center">
                                <div class="media align-items-center w-100">
                                    <div class="text-left">
                                        <h3 class="mb-0"> {{number_format(@$analytics->contacts['total'])}}  </h3>
                                        <span>Total Contacts


                                        </span>

                                    </div>
                                    <div class="img-icon bg-primary ml-auto">
                                        <i class="ti ti-tag text-white"></i>
                                    </div>
                                </div>
                        </div>
                        <div class="card-body d-flex align-itemes-center">
                            <div class="media align-items-center w-100">
                                <div class="text-left">
                                    <h3 class="mb-0">{{number_format(@$analytics->reviews['total'])}} </h3>
                                    <span>Total Reviews</span>
                                </div>
                                <div class="img-icon bg-pink ml-auto">
                                    <i class="ti ti-user text-white"></i>
                                </div>
                            </div>
                        </div>


                        <div class="card-body d-flex align-itemes-center">
                            <div class="media align-items-center w-100">
                                <div class="text-left">
                                    <h3 class="mb-0">{{number_format(@$analytics->avg_rating,1)}}  </h3>
                                    <span>Average rating</span>
                                </div>
                                <div class="img-icon bg-info ml-auto">
                                    <i class="ti ti-slice text-white"></i>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@if(empty(session('user_subscription')))
<div class="row">
    <div class="col-sm-12">
        <div class="card card-statistics">
            <div class="row">
                <div class="alert col-12 text-center">
                    <h1>Subscribe to unlock <strong>{{config('app.name')}}</strong> full potential</h1>

                    <div>
                            <a href="{{route('packages')}}" class="btn btn-lg btn-primary"><i  class="fa fa-unlock"></i> UNLOCK NOW</a>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
@endif

<div class="row">
        <div class="col-xxl-12 m-b-30">
                <div class="card card-statistics h-100 mb-0">
                    <div class="card-header">
                        <h4 class="card-title">Reviews Report (By Month)</h4>
                    </div>

                    <div class="chart-container">
                            <canvas id="chart-legend-top"></canvas>
                    </div>
                </div>


        </div>
</div>


    {{-- <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-xl-6 col-xxl-4">
                    <div class="card card-statistics">
                        <div class="card-header">
                            <h4 class="card-title">Conversion Visualizer</h4>
                        </div>
                        <div class="card-body pb-2">
                            <div class="row no-gutters">
                                <div class="col-sm-4 mt-3">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item px-0 pt-0 d-flex justify-content-between align-items-center">
                                            <p class="text-muted m-b-0">
                                                <i class="fa fa-circle-o mr-2 text-primary"></i>
                                                Direct
                                            </p>
                                            <p class="text-primary mb-0">44</p>
                                        </li>
                                        <li class="list-group-item d-flex px-0 justify-content-between align-items-center">
                                            <p class="text-muted m-b-0">
                                                <i class="fa fa-circle-o mr-2 text-warning"></i>
                                                Contacts
                                            </p>
                                            <p class="text-warning mb-0">55</p>
                                        </li>
                                        <li class="list-group-item d-flex px-0 justify-content-between align-items-center">
                                            <p class="text-muted m-b-0">
                                                <i class="fa fa-circle-o mr-2 text-info"></i>
                                                 Google
                                            </p>
                                            <p class="text-info mb-0">13</p>
                                        </li>
                                        <li class="list-group-item d-flex px-0 justify-content-between align-items-center">
                                            <p class="text-muted m-b-0">
                                                <i class="fa fa-circle-o mr-2 text-danger"></i>
                                                Facebook
                                            </p>
                                            <p class="text-danger mb-0">53</p>
                                        </li>
                                        <li class="list-group-item d-flex px-0 justify-content-between align-items-center">
                                            <p class="text-muted m-b-0">
                                                <i class="fa fa-circle-o mr-2 text-dark"></i>
                                                Zillow
                                            </p>
                                            <p class="text-dark mb-0">35</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-8">
                                    <div class="apexchart-wrapper">
                                        <div id="ecommercedemo5"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-xxl-4 m-b-30">
                    <div class="card card-statistics h-100 mb-0">
                        <div class="card-header">
                            <h4 class="card-title">Profile overview</h4>
                        </div>
                        <div class="card-body p-30">
                            <div class="row">
                                <div class="col-xxs-6 h-100 p-2 border-right border-bottom border-xxs-right-0">
                                    <div class="d-flex align-items-center justify-content-center">
                                        <div class="p-3 text-center">
                                            <a href="javascript:void(0);" class="btn btn-icon btn-round btn-inverse-primary"><i class="fe fe-settings"></i></a>
                                            <h2 class="m-t-20 mb-0">272</h2>
                                            <p class="text-muted d-block m-b-0">Daily Views</p>
                                            <span class="text-primary"> <i class="fe fe-activity"></i> 155.5% </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxs-6 h-100 p-2 border-bottom">
                                    <div class="d-flex align-items-center justify-content-center">
                                        <div class="p-3 text-center">
                                            <a href="javascript:void(0);" class="btn btn-icon btn-round btn-inverse-success"><i class="fe fe-user-check"></i></a>
                                            <h2 class="m-t-20 mb-0">450</h2>
                                            <p class="text-muted d-block m-b-0">Page Impressions</p>
                                            <span class="text-success"> <i class="fe fe-arrow-down-left"></i> 155.5% </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxs-6 h-100 p-2 border-right border-xxs-bottom border-xxs-right-0">
                                    <div class="d-flex align-items-center justify-content-center">
                                        <div class="p-3 text-center">
                                            <a href="javascript:void(0);" class="btn btn-icon btn-round btn-inverse-danger"><i class="fe fe-bar-chart-2"></i></a>
                                            <h2 class="m-t-20 mb-0">500</h2>
                                            <p class="text-muted d-block m-b-0">Total Views</p>
                                            <span class="text-danger"> <i class="fe fe-arrow-down-right"></i> 155.5% </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxs-6 h-100 p-2 ">
                                    <div class="d-flex align-items-center justify-content-center">
                                        <div class="p-3 text-center">
                                            <a href="javascript:void(0);" class="btn btn-icon btn-round btn-inverse-info"><i class="fe fe-crosshair"></i></a>
                                            <h2 class="m-t-20 mb-0">7525</h2>
                                            <p class="text-muted d-block m-b-0">Total Reviews</p>
                                            <span class="text-info"> <i class="fe fe-arrow-up"></i> 4.5 star </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- end row -->

@endsection



@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
<script src="https://www.chartjs.org/samples/latest/utils.js"></script>
<script>

		var color = Chart.helpers.color;
		function createConfig(legendPosition, colorName) {
			return {
				type: 'line',
				data: {
					labels:[
                    @if(isset($analytics->reviews_count))
                        @foreach($analytics->reviews_count as $key => $value)
                            '{{$key}}',

                        @endforeach
                    @endif
                    ],
					datasets: [{
						label: 'Reviews',
						data: [
                            @if(isset($analytics->reviews_count))
                                @foreach($analytics->reviews_count as $key => $value)
                                    '{{$value}}',
                                @endforeach
                            @endif
						],
						backgroundColor: color(window.chartColors[colorName]).alpha(0.5).rgbString(),
						borderColor: window.chartColors[colorName],
						borderWidth: 1
					}]
				},
				options: {
					responsive: true,
					legend: {
						position: legendPosition,
					},
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Month'
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Number of Reviews'
							}
						}]
					},
					title: {
						display: true,
						text: 'Review/Month Analysis: ' + legendPosition
					}
				}
			};
		}

		window.onload = function() {
			[{
				id: 'chart-legend-top',
				legendPosition: 'top',
				color: 'red'
			}].forEach(function(details) {
				var ctx = document.getElementById(details.id).getContext('2d');
				var config = createConfig(details.legendPosition, details.color);
				new Chart(ctx, config);
			});
		};
	</script>
@endpush
