@extends('layouts.master')
@push('styles')
    <style>
     .ck-editor__editable {
            min-height: 250px;
        }

        .btn-primary-a {
    color: #fff !important;
    background-color: #007bff !important;
    border-color: #007bff !important;
}
.btn-primary-d {
    color: #fff !important;
    background-color:#dc3545 !important;
    border-color: #dc3545 !important;
}

    </style>

@endpush

@section('content')
<main class="margin_main_container">
		<div class="user_summary">
			<div class="wrapper">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<figure>
                            <img src="{{@$user->avatar}}" alt="">
							</figure>
							<h1>{{@$user->fullname}}</h1>
							<span>
                                    {{@$user->profile['business_name']}} ,{{@$user->profile['country']}}
                            </span>
						</div>
						<div class="col-md-6">
							<ul>
								<li>
                                <strong>{{count(@$user->reviews)}}</strong>
                                <a href="#0" class="tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Reviews written by you"><i class="icon_star"></i>{{__('Reviews')}}</a>
								</li>
								{{-- <li>
									<strong>12</strong>
									<a href="#0" class="tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Number of people who have read your reviews"><i class="icon-user-1"></i> Reads</a>
								</li> --}}
								{{-- <li>
									<strong>36</strong>
									<a href="#0" class="tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Number of people who found your review useful"><i class="icon_like_alt"></i> Useful</a>
								</li> --}}
							</ul>
						</div>
					</div>
				</div>
				<!-- /container -->
			</div>
		</div>
		<!-- /user_summary -->
		<div class="container margin_60_35">
			<div class="row">
				<div class="col-lg-8">
                    @isset($user->reviews)
                        @if(count($user->reviews))
                            @foreach($user->reviews as $review)

                            <div class="review_card">
                                    <div class="row">
                                        <div class="col-md-2 user_info">
                                                <figure><img src="{{asset('img/avatar4.jpg')}}" alt=""></figure>
                                                <h5>{{__('Review:')}} "{{@$review['fullname']}}"</h5>
                                        </div>

                                        <div class="col-md-10 review_content">
                                            <div class="clearfix add_bottom_15">
                                                <span class="rating">
                                                    @if(@$review)
                                                        @for($i = 0; $i < $review['rating_score'];  $i++)
                                                            <i class="icon_star"></i>
                                                        @endfor
                                                    @endif
                                                    <em>{{number_format($review['rating_score'], 2)}}/5.00</em>
                                                </span>
                                                <em>{{__('Published:')}} {{when($review['created_at'])}}</em>
                                            </div>
                                            <h4>"{{$review['title']}}"</h4>
                                            <p>{{$review['review']}}</p>
                                            <ul >
                                                {{-- <li><a href="#0" class="btn_delete"><i class="icon-trash"></i>Delete</a></li> --}}
                                                <li  class="" style="margin-bottom:20px"><a class="w-100 reply btn-primary-a " href="#0"><i class="icon-edit-3 "></i> Reply</a></li>
                                            </ul>
                                            <div style="margin:30px auto;clear:both;display:none" class='reply-box' >
                                                <small class="text-muted">Please note that replies can not be edited. So make sure you double check before submitting.</small>
                                                    <form method='post' action="{{route('user.reply.store',$review['id'])}}">
                                                        @csrf
                                                        <textarea  rows="5"  class="terms" name="reply"></textarea>

                                                            <div class="form-group text-right">
                                                                <button style="margin-top:10px" class="btn btn-outline-primary btn-sm m-10">Send</button>
                                                            </div>
                                                    </form>

                                            </div>
                                        </div>



                                    </div>
                                    <!-- /row -->
                                    @if(count($review['replies']))
                                    @foreach($review['replies'] as $reply)
                                    <div class="row reply">
                                            <div class="col-md-2 user_info">
                                                <figure><img src="img/avatar.jpg" alt=""></figure>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="review_content">
                                                    <strong>Reply from {{@$user->profile['business_name']}}</strong>
                                                    <em>Published {{when($reply['created_at'])}}</em>
                                                    <p>
                                                        {!! $reply['reply'] !!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    @endif
                            </div>



                            @endforeach

                        @endif
                    @endisset


					<!-- /review_card -->

				</div>
				<!-- /col -->
				{{-- <div class="col-lg-4">
					<div class="box_general general_info">
						<h3>Delete a review<i class="pe-7s-help1"></i></h3>
						<p><strong>Mucius doctus constituto pri at.</strong> At vix utinam corpora, ea oblique moderatius usu. Vix id viris consul honestatis, an constituto deterruisset consectetuer pro quo corrumpit euripidis...<br><strong><a href="faq.html">Rear more</a></strong></p>
						<hr>
						<h3>Post a review<i class="pe-7s-help1"></i></h3>
						<p>Dolor detraxit duo in, ei sea dicit reformidans. Mel te accumsan patrioque referrentur. Has causae perfecto ut, ex choro assueverit eum...<br><strong><a href="faq.html">Rear more</a></strong></p>
						<hr>
						<h3>Approve a review<i class="pe-7s-help1"></i></h3>
						<p>Sed ne prompta insolens mediocrem, omnium fierent sed an, quod vivendo mel in. Argumentum honestatis ad mel, cu vis quot utroque...<br><strong><a href="faq.html">Rear more</a></strong></p>
						<hr>
						<div class="text-center"><a href="faq.html" class="btn_1 small">View al Faq</a></div>
					</div>
				</div> --}}
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</main>
@endsection


@push('scripts')
<script src="https://cdn.ckeditor.com/ckeditor5/12.0.0/classic/ckeditor.js"></script>
    <script>
        var classes = document.querySelectorAll( '.terms' );
        for($i= 0; $i < classes.length; $i++){
            var editor = ClassicEditor.create( classes[$i] );
        }

        $('.reply').click(function(){
            $(this).parents('ul').siblings('.reply-box').toggle();
            $(this).toggleClass('btn-primary-a btn-primary-d');
        });
    </script>

@endpush
