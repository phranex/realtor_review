@extends('layouts.realtor')


@section('content')
    <!-- begin row -->
    {{--<div class="row">--}}
        {{--<div class="col-md-12 m-b-30">--}}
            {{--<!-- begin page title -->--}}
            {{--<div class="d-block d-sm-flex flex-nowrap align-items-center">--}}
                {{--<div class="page-title mb-2 mb-sm-0">--}}
                    {{--<h1>Account Settings</h1>--}}
                {{--</div>--}}
                {{--<div class="ml-auto d-flex align-items-center">--}}
                    {{--<nav>--}}
                        {{--<ol class="breadcrumb p-0 m-b-0">--}}
                            {{--<li class="breadcrumb-item">--}}
                                {{--<a href="index.html"><i class="ti ti-home"></i></a>--}}
                            {{--</li>--}}
                            {{--<li class="breadcrumb-item">--}}
                                {{--Pages--}}
                            {{--</li>--}}
                            {{--<li class="breadcrumb-item active text-primary" aria-current="page">Account Settings</li>--}}
                        {{--</ol>--}}
                    {{--</nav>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- end page title -->--}}
        {{--</div>--}}
    {{--</div>--}}
    <!-- end row -->

    <!--mail-Compose-contant-start-->
    <div class="row account-contant">
        <div class="col-12">
            <div class="card card-statistics">
                <div class="card-body p-0">
                    <div class="row no-gutters">
                        <div class="col-xl-3 pb-xl-0 pb-5 border-right">
                            <div class="page-account-profil pt-5">
                                <div class="profile-img text-center rounded-circle">
                                    <div class="pt-5">
                                        <div class="bg-img m-auto">
                                        <img src="{{displayPhoto($user->avatar)}}" class="img-fluid" alt="users-avatar">
                                        </div>
                                        <div class="profile pt-4">
                                            <h4 class="mb-1">{{$user->fullname}}</h4>
                                            <p>{{@$profile->title}}</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="py-5 profile-counter">
                                    <ul class="nav justify-content-center text-center">
                                        <li class="nav-item border-right px-3">
                                            <div>
                                                <h4 class="mb-0">{{count($user->invitations)}}</h4>
                                                <p>Invitations</p>
                                            </div>
                                        </li>

                                        <li class="nav-item border-right px-3">
                                            <div>
                                                <h4 class="mb-0">{{count($user->contacts)}}</h4>
                                                <p>Contacts</p>
                                            </div>
                                        </li>

                                        <li class="nav-item px-3">
                                            <div>
                                                <h4 class="mb-0">{{count($user->reviews)}}</h4>
                                                <p>Reviews</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="profile-btn text-center">
                                <form action="{{route('user.upload-avatar')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div><label for='avatar' class="btn avatar btn-light text-primary mb-2">Change Avatar</label>
                                    <input style="opacity:0" type="file" id='avatar' name='avatar' />
                                    <input style="display:none" type="submit" class="btn btn-xs" id="upload-btn" value="Upload" />
                                    </form></div>
                                    {{-- <div><button class="btn btn-danger">Delete</button></div> --}}
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-5 col-md-6 col-12 border-t border-right">
                            <div class="page-account-form">
                                <div class="form-titel border-bottom p-3">
                                    <h5 class="mb-0 py-2">Edit Your Personal Settings</h5>
                                </div>
                                <div class="p-4">
                                    <form method="post" action="{{route('user.profile')}}" enctype="multipart/form-data">
                                        @csrf

                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="name1">Full Name</label>
                                                <input type="text" class="form-control" id="name" readonly required name="name" value="{{$user->fullname}}">
                                            </div>
                                            <div class="form-group col-md-12">
                                                    <label for="title1">Business Name</label>
                                                <input type="text" class="form-control" required name="business_name" id="business_name" value="@if(old('business_name')) {{old('business_name')}} @else {{@$profile->business_name}} @endif">
                                                </div>
                                            <div class="form-group col-md-12">
                                                <label for="title1">Title</label>
                                            <input type="text" class="form-control" required name="title" id="title" value="@if(old('title')) {{old('title')}} @else {{@$profile->title}} @endif">
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="phone1">Phone Number</label>
                                                <input type="text" class="form-control" required name="phone_number" id="phone" value="@if(old('phone_number')) {{old('phone_number')}} @else {{@$profile->phone_number}} @endif">
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="email1">Email</label>
                                                <input type="email" class="form-control" required id="email" name="email" value="@if(old('email')) {{old('email')}} @else {{@$profile->email}} @endif">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="add1">Address</label>
                                            <input type="text" class="form-control" required id="add1" name="address" value="@if(old('address')) {{old('address')}} @else {{@$profile->address}} @endif">
                                        </div>
                                        <div class="form-group">
                                            <label for="add2">Address 2</label>
                                            <input type="text" class="form-control"  id="add2" name="address2" value="@if(old('address2')) {{old('address2')}} @else {{@$profile->address2}} @endif">
                                        </div>

                                        <div class="form-group">

                                                <label class="mb-1">Birthday</label>

                                                <input type="date" name="date_of_birth" value="@if(old('date_of_birth')){{old('date_of_birth')}}@else{{@$profile->date_of_birth}}@endif" required class="datepicker form-control w-100" />

                                        </div>
{{--                                        <div class="form-row">--}}
{{--                                            <div class="form-group col-md-6">--}}
{{--                                                <label for="inputState">City</label>--}}
{{--                                                <select required id="city" class="form-control" name="city">--}}
{{--                                                    <option>Choose...</option>--}}
{{--                                                    <option selected="">London</option>--}}
{{--                                                    <option>Montreal</option>--}}
{{--                                                    <option>Delhi</option>--}}
{{--                                                    <option>Tokyo</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                            <div class="form-group col-md-6">--}}
{{--                                                <label for="inputState">State</label>--}}
{{--                                                <select required id="state" class="form-control" name="state">--}}
{{--                                                    <option>Choose...</option>--}}
{{--                                                    <option selected="">England</option>--}}
{{--                                                    <option>California </option>--}}
{{--                                                    <option>Texas</option>--}}
{{--                                                    <option>Scotland </option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                            <div class="form-group col-md-6">--}}
{{--                                                    <label for="inputState">Country</label>--}}
{{--                                                    <select required id="state" class="form-control" name="country">--}}
{{--                                                        <option>Choose...</option>--}}
{{--                                                        <option selected="">England</option>--}}
{{--                                                        <option>California </option>--}}
{{--                                                        <option>Texas</option>--}}
{{--                                                        <option>Scotland </option>--}}
{{--                                                    </select>--}}
{{--                                                </div>--}}
{{--                                            <div class="form-group col-md-6">--}}
{{--                                                <label for="inputZip">Zip</label>--}}
{{--                                                <input required type="text" class="form-control" id="zip" name="zip" value="EC1A 1BB">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <div class="form-check">--}}
{{--                                                <input class="form-check-input" type="checkbox" id="gridCheck">--}}
{{--                                                <label class="form-check-label" for="gridCheck">--}}
{{--                                                    I agree to receive email notification.--}}
{{--                                                </label>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

                                        <button type="submit" class="btn btn-primary submit">Update Information</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 border-t col-12">
                        @isset($socialTypes)

                            @if(count($socialTypes) || count($socials))

                                    <div class="page-account-form">
                                        <div class="form-titel border-bottom p-3">
                                            <h5 class="mb-0 py-2">Your External Links</h5>
                                        </div>
                                        <div class="p-4">
                                            <form method="post" action="{{route('user.social.store')}}">
                                                @csrf
                                                @foreach ($socials as $item)

                                                    <div class="form-group">
                                                        <label for="fb">{{ucwords($item['socialLinkType']['name'])}} URL:</label>
                                                    <input type="text" class="form-control" @if(@$item['socialLinkType']['isRequired']) required @endif  name="{{$item['socialLinkType']['name']}}" value="{{$item['link']}}">
                                                    </div>
                                                @endforeach
                                                @foreach ($socialTypes as $item)
                                                    <div class="form-group">
                                                        <label for="fb">{{ucwords($item['name'])}} URL:</label>
                                                    <input type="text" class="form-control" @if(@$item['isRequired']) required @endif  name="{{$item['name']}}" value="">
                                                    </div>
                                                @endforeach



                                                <button type="submit" class="btn btn-primary">Save & Update</button>
                                            </form>
                                        </div>
                                    </div>



                            @endif
                        @endisset
                        <div class="form-titel border-bottom p-3">
                                <h5 class="mb-0 py-2">Change Password</h5>
                        </div>
                        <form method="post"  action='{{route('auth.change-password')}}' class="form-horizontal p-4 form-material">
                                @csrf
                                    <div class="  form-group">
                                    <label class="col-md-12">{{trans('Old Password')}}</label>
                                        <div class="col-md-12">
                                        <input type="password" name ='old_password'  class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">{{trans('New Password')}}</label>
                                        <div class="col-md-12">
                                            <input type="password" name='password' class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">{{trans('Confirm Password')}}</label>
                                        <div class="col-md-12">
                                            <input type="password" value="password" name='password_confirmation' class="form-control form-control-line">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button class="btn btn-success">{{trans('Change Password')}}</button>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--mail-Compose-contant-end-->
    @endsection

   @push('scripts')
    <script>

        $(document).ready(function(){
            $('#avatar').change(function(){
                if($(this).val() != ''){

                    $('label.avatar').text(one_file_selected);
                    $('#upload-btn').show();
                }else{
                    $('label.avatar').text('Change Avatar');
                    $('#upload-btn').hide();
                }
            });
        });
    </script>
   @endpush
