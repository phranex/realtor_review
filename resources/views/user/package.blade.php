@extends('layouts.realtor')
@push('styles')
    <style>
    .heading {
  font-size: 25px;
  margin-right: 25px;
}

.fa {
  font-size: 25px;
}

.checked {
  color: orange;
}

/* Three column layout */
.side {
  float: left;
  width: 15%;
  margin-top: 10px;
}

.middle {
  float: left;
  width: 70%;
  margin-top: 10px;
}

/* Place text to the right */
.right {
  text-align: right;
}

/* Clear floats after the columns */
.r-row:after {
  content: "";
  display: table;
  clear: both;
}

/* The bar container */
.bar-container {
  width: 100%;
  background-color: #f1f1f1;
  text-align: center;
  color: white;
}

/* Individual bars */
.bar-5 {width: 60%; height: 18px; background-color: #4CAF50;}
.bar-4 {width: 30%; height: 18px; background-color: #2196F3;}
.bar-3 {width: 10%; height: 18px; background-color: #00bcd4;}
.bar-2 {width: 4%; height: 18px; background-color: #ff9800;}
.bar-1 {width: 15%; height: 18px; background-color: #f44336;}

/* Responsive layout - make the columns stack on top of each other instead of next to each other */
@media (max-width: 400px) {
  .side, .middle {
    width: 100%;
  }
  /* Hide the right column on small screens */
  .right {
    display: none;
  }
}

    </style>
@endpush


@section('content')
<div class="row">
    <div class="col-12 ">
            <div class="card card-statistics">
                <div class="card-header">
                    <div class="card-heading">
                        <h4 class="card-title">{{config('app.name')}} Pricing and Plans
                        {{-- <Small class="text-bold">Total: {{count($users)}}</Small> --}}
                        </h4>
                        {{-- <div class="text-right">
                            <a class="btn btn-sm btn-primary" href="">Add Package</a>
                        </div> --}}
                    </div>
                </div>

            </div>
        </div>
</div>

<div class="row">
    @isset($packages)

        @if(count((array) $packages))
            @foreach($packages as $package)
            <div  class="col-xl-3 col-md-6">
                <div  class="card card-statistics text-center py-3">
                    <div class="card-body pricing-content">
                        <div class="pricing-content-card">
                        <h5>{{$package['name']}}</h5>
                            <h2 class="text-primary pt-3">{{currencyConverter($package['price'])}}</h2>
                            <p class="text-primary pb-3">/ Monthly</p>
                            <div class="text-center">Features</div>
                            <hr/>
                            <ul class="py-2">
                                @if(count($package['qualities']))
                                    @foreach($package['qualities'] as $quality)
                                        <li>
                                            @if($quality['feature']['type'] == 1)
                                                <strong>
                                                    @if($quality['value'] >= 100000)
                                                        Unlimited
                                                    @else
                                                    {{$quality['value']}}
                                                    @endif
                                                </strong> {{$quality['feature']['name']}}
                                            @else
                                                @if($quality['value'])
                                                    <i class="fa fa-check text-success"></i>  {{$quality['feature']['name']}}
                                                @else
                                                    <i class="fa fa-times text-danger"></i>   {{$quality['feature']['name']}}
                                                @endif
                                            @endif
                                        </li>
                                    @endforeach
                                @endif

                            </ul>

                            @if(!empty(session('user_subscription')))
                                @if(strtolower(session('user_subscription.name')) != strtolower($package['name']))
                                    <div class="pt-2">
                                    <a href="{{route('package.preview',$package['id'])}}" class="btn btn-outline-primary btn-sm"> Upgrade</a>
                                    </div>
                                @else
                                <div class="pt-2">
                                        <a href="#" class="btn btn-success  btn-sm"> Active Plan</a>
                                </div>
                                @endif
                            @else
                                <div class="pt-2">
                                    <a href="{{route('package.preview',$package['id'])}}" class="btn btn-outline-primary btn-sm"> Upgrade</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        @endif


    @endisset
</div>


@endsection



@push('scripts')

@endpush
