
@extends('layouts.master')

@section('content')
<main>
		<div class="reviews_summary">
			<div class="wrapper">
				<div class="container">
					<div class="row">
						<div class="col-lg-8">
							<figure>
								<img src="{{@$user->avatar}}" alt="">
							</figure>
							<small>{{__('Realtor')}}</small>
							<h1>{{@$user->profile['business_name']}}</h1>
							<span class="rating">
                                @for($i = 0; $i < $total_rating_score;  $i++)
                                    <i class="icon_star"></i>
                                @endfor
                                @for($i = 0; $i < 5 - $total_rating_score;  $i++)
                                    <i class="icon_star empty"></i>
                                @endfor

                                <em>{{number_format($total_rating_score, 2)}}/5.00 - based on {{$num_of_reviews}} reviews</em>
                            </span>
						</div>
						{{-- <div class="col-lg-4 review_detail">
							<div class="row">
								<div class="col-lg-9 col-9">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width: 90%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
								<div class="col-lg-3 col-3 text-right"><strong>5 stars</strong></div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-lg-9 col-9">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
								<div class="col-lg-3 col-3 text-right"><strong>4 stars</strong></div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-lg-9 col-9">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
								<div class="col-lg-3 col-3 text-right"><strong>3 stars</strong></div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-lg-9 col-9">
									<div class="progress">
										<div class="progress-bar" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
								<div class="col-lg-3 col-3 text-right"><strong>2 stars</strong></div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-lg-9 col-9">
									<div class="progress last">
										<div class="progress-bar" role="progressbar" style="width: 0" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
								<div class="col-lg-3 col-3 text-right"><strong>1 stars</strong></div>
							</div>
							<!-- /row -->
						</div> --}}
					</div>
				</div>
				<!-- /container -->
			</div>
		</div>
		<!-- /reviews_summary -->

		<div class="container margin_60_35">
			<div class="row">
				<div class="col-lg-8">
                        {{-- <a href="{{route('review.create',base64_encode($user->profile['business_name']."-".$user->id))}}" class="btn_1 float-right small btn-xs">{{__('Write a Review')}}</a> --}}
                        <div style="clear:both;margin-bottom:25px" class=""></div>
					{{-- <div class="review_card"> --}}

						{{-- <div class="row">
							<div class="col-md-2 user_info">
								<figure><img src="img/avatar1.jpg" alt=""></figure>
								<h5>Marika</h5>
							</div>
							<div class="col-md-10 review_content">
								<div class="clearfix add_bottom_15">
									<span class="rating"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star empty"></i><em>4.50/5.00</em></span>
									<em>Published 54 minutes ago</em>
								</div>
								<h4>"Avesome Experience"</h4>
								<p>Eos tollit ancillae ea, lorem consulatu qui ne, eu eros eirmod scaevola sea. Et nec tantas accusamus salutatus, sit commodo veritus te, erat legere fabulas has ut. Rebum laudem cum ea, ius essent fuisset ut. Viderer petentium cu his. Tollit molestie suscipiantur his et.</p>
								<ul>
									<li><a href="#0"><i class="icon_like_alt"></i><span>Useful</span></a></li>
									<li><a href="#0"><i class="icon_dislike_alt"></i><span>Not useful</span></a></li>
									<li><span>Share</span> <a href="#0"><i class="ti-facebook"></i></a> <a href="#0"><i class="ti-twitter-alt"></i></a> <a href="#0"><i class="ti-google"></i></a></li>
								</ul>
                            </div> --}}
                            @isset($user->reviews)
                            @if(count($user->reviews))
                                @foreach($user->reviews as $review)
                                <div class="review_card">

                                        <div class="row">
                                            <div class="col-md-2 user_info">
                                                    <figure><img src="{{asset('img/avatar4.jpg')}}" alt=""></figure>
                                                    <h5>{{__('Reviewed by:')}} "{{@$review['fullname']}}"</h5>
                                                </div>

                                            <div class="col-md-10 review_content">
                                                <div class="clearfix add_bottom_15">
                                                    <span class="rating">
                                                        @if(@$review)
                                                            @for($i = 0; $i < $review['rating_score'];  $i++)
                                                                <i class="icon_star"></i>
                                                            @endfor
                                                            @for($i = 0; $i < 5 - $review['rating_score'];  $i++)
                                                            <i class="icon_star empty"></i>
                                                        @endfor
                                                        @endif
                                                        <em>{{number_format($review['rating_score'], 2)}}/5.00</em>
                                                    </span>
                                                    <em>{{__('Published:')}} {{when($review['created_at'])}}</em>
                                                </div>
                                                <h4>"{{$review['title']}}"</h4>
                                                <p>{{$review['review']}}</p>
                                                <ul >
                                                    {{-- <li><a href="#0" class="btn_delete"><i class="icon-trash"></i>Delete</a></li> --}}
                                                    {{-- <li style="margin-bottom:20px"><a href="#0"><i class="icon-edit-3"></i> Edit</a></li> --}}
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- /row -->
                                        <!-- /row -->
                                        @if(count($review['replies']))
                                        @foreach($review['replies'] as $reply)
                                        <div class="row reply">
                                                <div class="col-md-2 user_info">
                                                    <figure><img src="img/avatar.jpg" alt=""></figure>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="review_content">
                                                        <strong>Reply from {{@$user->profile['business_name']}}</strong>
                                                        <em>Published {{when($reply['created_at'])}}</em>
                                                        <p>
                                                            {!! $reply['reply'] !!}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        @endif
                            <!-- /reply -->

                                </div>
                                @endforeach

                            @endif
                            @endisset


					{{-- </div> --}}

					<!-- /review_card -->
					{{-- <div class="pagination__wrapper add_bottom_15">
						<ul class="pagination">
							<li><a href="#0" class="prev" title="previous page">❮</a></li>
							<li><a href="#0" class="active">1</a></li>
							<li><a href="#0">2</a></li>
							<li><a href="#0">3</a></li>
							<li><a href="#0">4</a></li>
							<li><a href="#0" class="next" title="next page">❯</a></li>
						</ul>
					</div> --}}
					<!-- /pagination -->
				</div>
				<!-- /col -->
				<div class="col-lg-4">
					<div class="box_general company_info">
						<h3>{{$user->profile['business_name']}}</h3>
						<p>{{@$user->profile['description']}}</p>
						<p><strong>{{__('Address')}}</strong><br>{{@$user->profile['address']}}<br>{{@$user->profile['city']}} - {{@$user->profile->country}}</p>
						<p><strong>{{__('Website')}}</strong><br><a href="{{@$user->profile['website']}}">{{@$user->profile['website']}}</a></p>
						<p><strong>{{__('Email')}}</strong><br><a href="{{@$user->profile['email']}}">{{@$user->profile['email']}}</a></p>
						<p><strong>{{__('Telephone')}}</strong><br>{{@$user->profile['phone_number']}}</p>
                        <p class="follow_company"><strong>{{__('Follow us')}}</strong><br>
                            @if(count(@$user->social))
                                @foreach($user->social as $social)
                                    <a href="{{$social['link']}}" target="_blank"><i class="social_{{$social['socialLinkType']['name']}}_circle"></i></a>
                                @endforeach

                            @endif

                            {{-- <a href="#0"><i class="social_twitter_circle"></i></a>
                            <a href="#0"><i class="social_googleplus_circle"></i></a>
                            <a href="#0"><i class="social_instagram_circle"></i></a> --}}
                        </p>
					</div>
				</div>
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->

	</main>


@endsection
