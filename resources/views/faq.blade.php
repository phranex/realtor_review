@extends('layouts.master')


@section('content')

<main style="transform: none;">
    <section class="hero_single general">
        <div class="wrapper">
            <div class="container">
                <i class="pe-7s-help1"></i>
                <h1>{{config('app.name')}} Faq Center</h1>
                <p>{{config('app.name')}} helps grow your business using customer reviews</p>
            </div>
        </div>
    </section>
    <!-- /hero_single -->

     <div class="container margin_60_35" style="transform: none;">
        <div class="row" style="transform: none;">

            <!--/aside -->

            <div class="col-lg-9 mx-auto" id="faq">
                <h4 class="nomargin_top text-center">Frequently Asked Questions</h4>
                <div role="tablist" class="add_bottom_45 accordion_2" id="payment">
                        @if(isset($faqs))
                            @if(count($faqs))
                                @foreach($faqs as $key => $value)
                                    <div class="card">
                                        <div class="card-header" role="tab">
                                            <h5 class="mb-0 toggleChevron">
                                            <a data-toggle="collapse" href="#faq{{$loop->iteration}}" aria-expanded="true"><i class="indicator @if($loop->iteration == 1) ti-minus @else ti-plus @endif"></i>{{$key}}</a>
                                            </h5>
                                        </div>

                                        <div id="faq{{$loop->iteration}}" class="collapse @if($loop->iteration == 1)show @endif" role="tabpanel" data-parent="#payment">
                                            <div class="card-body">
                                            <p>{{$value}}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        @endif

                </div>

            </div>
            <!-- /col -->
        </div>
        <!-- /row -->
    </div>
    <!--/container-->
</main>
@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $('.toggleChevron').click(function(){
                $(this).find('i.indicator').toggleClass('ti-plus ti-minus');
            });
        });
    </script>

@endpush
