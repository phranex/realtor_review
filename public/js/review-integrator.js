var review_box = document.getElementById('review-db-box');

var font_awesome_cdn = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css';
var styles = 'https://realtor.test/css/review-integration.css';
addLinkReference(font_awesome_cdn);
addLinkReference(styles);
var scripts = document.getElementsByTagName('script');
script = scripts[scripts.length -1];
var realtor = script.getAttribute('data-author');
var url = 'https://realtor-api.dockay.ng/api/reviews/integrations/get?identifier='+realtor;
var url2 = 'https://realtor-api.dockay.ng/api/reviews/integrations/get';
var reviews ;


var data = {
    identifier : realtor
};
var options = {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        origin: 'https://realtor.test',
        headers: {
            'Content-Type': 'application/json',
            'access-control-allow-origin': '*'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },

        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        // body: JSON.stringify(data), // body data type must match "Content-Type" header
}




// var reviews = 5;


    getReviews(url, options =null, review_box);
    // html += insertReviewCard(reviews,review_card);



//   html +=   '</div></div>'; // end of divs



// review_box.innerHTML = html;



function addLinkReference (link_cdn) {

    var link = document.createElement("link");
    var head = document.head;

    link.rel = 'stylesheet';
    link.href = link_cdn;
    head.insertAdjacentElement('beforeend',link);
}

function insertReviewCard(review){
    var h = '';
    if(review.length){

        for(i = 0; i < review.length; i++){
            h += `<div class="row" >
            <div class="rev-box">
        <div class=' rev rev-profile-box'>
            <div class=" rev-p rev-img-holder">
                    <img class='img-resp img-rev' src="https://realtor.test/img/avatar.jpg"  />
            </div>
            <div class="rev-p profile">

                <p>${review[i].fullname}</p>
            </div>

        </div>
        <div class=" rev rev-box-content">`;
        h += ratingScore(review[i].rating_score);

        h += ` <p style='font-weight:bold'>${review[i].title}</p>
                        <p>${review[i].review}</p>

                        <div class="time-posted">
                            <small>${review[i].when}</small>
                        </div>
                </div>
            </div>
            </div>

            <hr style="border:3px solid #f1f1f1">`;
        }
    }

    return h;
}

function ratingScore(rating) {
    var q = '';

    if(rating){
        for(j = 0; j < rating; j++){
            q += ' <span class="fa fa-star checked"></span>';
        }
        for(k = 0; k < 5 -rating; k++){
            q += ' <span class="fa fa-star"></span>';
        }


    }
    return q;
}


async function  getReviews(url, options = null, box){

    return await fetch(url, options)
            .then(response => {
               return  response.json();
                // console.log(response.body);
            })
            .then((result) => {
                if(result.status == 1){
                    reviews = result.data.reviews;
                    console.log(result.data);
                    html = rev_container(result.data.ratings);
                     html += insertReviewCard(result.data.reviews);
                     html += `<div class="rev-pagination">`;
                     if(result.data.pagination.total){
                        if(result.data.pagination.previous_page > 1) {
                            previous_index = result.data.pagination.previous_page - 1;
                            data_href = `data-href =${url2}?page=${previous_index}`;
                        }else{
                           data_href = ``;
                        }
                        html +=  ` <a  href="#" onclick=getPage(this.getAttribute('data-href'),${options}) ${data_href} href='#'>&laquo;</a>`;
                         for(i = 0; i < result.data.pagination.total / result.data.pagination.limit ; i++){
                             j = i + 1;
                             var active = 'n';
                             if(result.data.pagination.previous_page == i) {
                                active = 'active';
                                console.log(i);
                                active_index = i + 1;
                           }
                           html +=  `<a class=${active} onclick=getPage(this.getAttribute('data-href'),${options}) data-href=${url2}?page=${j} href='#'>${j}</a>`;
                         }
                         if(active_index >= result.data.pagination.total ) {
                            disabled = 'disabled = true';
                            data_href = ``;
                       } else {
                           disabled = '';
                           data_href = `data-href = ${url2}?page=${active_index + 1}`;
                       }
                        html +=  ` <a ${disabled} href="#" onclick=getPage(this.getAttribute('data-href'),${options}) ${data_href} href='#'>&raquo;</a>`;
                     }



                     html += '</div></div>';

                     box.innerHTML = html;
                }
            });


}

function getPage(url, options= null){

    var url = url;
    var review_holder = document.getElementById('holder');
    // alert(url);
    fetch(url, options)
            .then(response => {
               return  response.json();
                // console.log(response.body);
            })
            .then((result) => {
                if(result.status == 1){
                    html ='';
                    reviews = result.data.reviews;
                    console.log(result.data);
                     html += insertReviewCard(result.data.reviews);
                     html += `<div class="rev-pagination">`;
                     if(result.data.pagination.total){
                         if(result.data.pagination.previous_page > 1) {
                             previous_index = result.data.pagination.previous_page - 1;
                             data_href = `data-href = ${url2}?page=${previous_index}`;
                         }else{
                            data_href = `data-href = ${url2}?page=1`;
                         }
                         html +=  ` <a  href="#" onclick=getPage(this.getAttribute('data-href'),${options}) ${data_href} href='#'>&laquo;</a>`;
                         for(i = 0; i < result.data.pagination.total / result.data.pagination.limit ; i++){
                             j = i + 1;
                             var active = 'n';
                             if(result.data.pagination.previous_page == i) {
                                 active = 'active';
                                 active_index = i + 1;
                            }
                           html +=  `<a class=${active} onclick=getPage(this.getAttribute('data-href'),${options}) data-href=${url2}get?page=${j} href='#'>${j}</a>`;
                         }
                         if(active_index >= result.data.pagination.total ) {

                             data_href = '';
                        } else {

                            data_href = `data-href = ${url2}get?page=${active_index + 1}`;
                        }
                         html +=  ` <a ${disabled} href="#" onclick=getPage(this.getAttribute('data-href'),${options}) ${data_href} href='#'>&raquo;</a>`;
                     }



                     html += '</div></div>';

                     review_holder.innerHTML = html;
                }
            });




}



function rev_container(result){
    var html = `<div class="rev-container">
    <span class="heading">User Rating</span>`;

    html += ratingScore(result.avg_rating);

    html += `

    <p><span id='avg_rating'>${result.avg_rating}</span> average rating based on <span id='total_rev'>${result.total}</span> reviews.</p>
    <hr style="border:3px solid #f1f1f1">

        <div class="row">`;
    html += ratingScale(result);

        html += `</div>
        <div class="row">
            <h5>Reviews</h5>
            <hr style="border:3px solid #f1f1f1">
        </div>
        <div id='holder' class="holder">`;

        return html;

}


function ratingScale(scores){

    var rating_max = 5;
    html = '';

    for(i = rating_max; i > 0; i--){
        var obj = scores.rating_scores_count.find(x => x.rating_score == i);
        width = 0;
        if(obj){
            avg = obj.count /scores.total  * 100;
            width = avg + '%';
            html += ` <div class="side">
                        <div>${i} star</div>
                    </div>
                    <div class="middle">
                        <div class="bar-container">
                        <div style='width:${width}' class="bar-${i}"></div>
                        </div>
                    </div>
                    <div class="side right">
                        <div>${obj.count}</div>
                    </div>`;
        }else{
            html += ` <div class="side">
                        <div>${i} star</div>
                    </div>
                    <div class="middle">
                        <div class="bar-container">
                        <div style='width:${width}' class="bar-${i}"></div>
                        </div>
                    </div>
                    <div class="side right">
                        <div>0</div>
                    </div>`;
        }
    }

    return html;

}









