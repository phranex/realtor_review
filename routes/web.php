<?php
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use \App\Library\Consume;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//homepage
Route::get('/', function () {
    if(session()->has('user'))
        return redirect()->route('user.profile');
    //get latest reviews
    $client = Consume::getInstance();
    $url = 'reviews/?limit=5
    ';
    $response = $client->getResponse('get',$url);
    $reviews = null;
    $index = null;
    if($response['status']){
        $reviews = $response['data'];
    }


    $url_page = 'page-setup/index';
    $response_page = $client->getResponse('get',$url_page);

    if($response_page['status'] == 1){
        $index = (array) $response_page['data']->setting;

    }
    logger($response);
    logger($response_page);

    return view('welcome',compact('reviews', 'index'));

    // return throwError($response);
})->name('landing-page');


//realtors landing page

Route::get('/realtors', function (){
    $client = Consume::getInstance();
    $url_page = 'page-setup/index';
    $response_page = $client->getResponse('get',$url_page);
    $index = null;
    $features = null;

    //get features
    $url_page = 'page-setup/features';
    $response = $client->getResponse('get',$url_page);


    if($response_page['status'] == 1){
        $index = (array) $response_page['data']->setting;

    }
    if($response['status'] == 1){
        $features = (array) $response['data']->setting;

    }

    logger($response_page);
    return view('welcome-realtor', compact('index', 'features'));
})->name('realtor.landing-page');


Route::get('/faq', function (){
    $client = Consume::getInstance();
    $url_page = 'page-setup/faq';
    $response_page = $client->getResponse('get',$url_page);
    $faqs = null;
    if($response_page['status'] == 1){
        $faqs = (array) $response_page['data']->setting;

    }

    logger($response_page);
    return view('faq', compact('faqs'));
})->name('faq');


Route::get('/policy', function (){
    $client = Consume::getInstance();
    $url_page = 'page-setup/policy';
    $response_page = $client->getResponse('get',$url_page);
    $policy = null;
    if($response_page['status'] == 1){
        $policy = $response_page['data']->setting['editordata'];

    }

    logger($response_page);
    return view('policy', compact('policy'));
})->name('policy');

Route::get('/terms', function (){
    $client = Consume::getInstance();
    $url_page = 'page-setup/terms';
    $response_page = $client->getResponse('get',$url_page);
    $terms = null;
    if($response_page['status'] == 1){
        $terms = $response_page['data']->setting['editordata'];

    }

    logger($response_page);
    return view('terms', compact('terms'));
})->name('terms');


Route::get('/pricing', function(){
    $client = Consume::getInstance();
        $url = 'packages/get';
       $response = $client->getResponse('get',$url);
       $packages = [];
        if($response['status'] == 1){
            $packages = $response['data'];
        }

    return view('pricing',compact('packages'));
})->name('pricing');


Route::post('subscribe', function () {
    request()->validate([
        'email' => 'required'
    ]);

    $email = request('email');

    $client = Consume::getInstance();
    $url = 'subscription/store';
    $data = [
        'email' => $email
    ];
    $response = $client->getResponse('post',$url,$data);
    if($response['status'] == 1){
        return back()->with('success', trans('Subscription was successful'));
    }elseif($response['status'] == 56){
        return back()->with('error', $response['message']);
    }

    return back()->with('error', trans('Subscription was unsuccessful'));
})->name('subscribe');

Route::post('user/register', 'AuthController@register')->name('user.register');
Route::post('user/profile','UserController@profilestore')->name('user.profile');
Route::post('user/external','UserController@socialStore')->name('user.social.store');
Route::post('user/login', 'AuthController@login')->name('user.login');
Route::get('/logout', function(){
        userLogout();
        return back();
})->name('user.logout');

Auth::routes();

Route::get('/facebook', function(){
    $url = "https://www.facebook.com/dialog/oauth?client_id=".env('FACEBOOK_ID')."&redirect_uri=".route('facebook.test')."&scope=manage_pages&state=&type_id=".request('id');
    return redirect($url);
})->name('facebook.integration');

Route::get('/facebook/test', function(){

    $url = "https://graph.facebook.com/oauth/access_token?client_id=".env('FACEBOOK_ID')."&client_secret=".env('FACEBOOK_SECRET')."&code=".request('code')."&redirect_uri=".route('facebook.test');
    // $response = redirect($url);
   $response = file_get_contents($url);
   $resp = json_decode($response, true);
   $short_access_token = $resp['access_token'];

   $url_get_long_access_token = "https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=".env('FACEBOOK_ID')."&client_secret=".env('FACEBOOK_SECRET')."&fb_exchange_token=".$short_access_token."&redirect_uri=".route('facebook.test');
   $response_long = file_get_contents($url_get_long_access_token);
  $resp_long = json_decode($response_long, true);
   $long_access_token = $resp_long['access_token'];

   $url_page_access_token = "https://graph.facebook.com/me/accounts?access_token=".$long_access_token."&redirect_uri=".route('facebook.test');
   $response_page_token = file_get_contents($url_page_access_token);
    $resp_page = json_decode($response_page_token, true);
    session(['user_fb_pages' => $resp_page['data']]);
    return redirect(session('fallback'));
    //    $page = $resp_page['data'][0];
    //    $page_access = $page['access_token'];
    //    $page_id = $page['id'];


    //    $url_get_ratings = "https://graph.facebook.com/StoneArmsInc/ratings?access_token=".$page_access;
    //    $url_get_ratings = "https://graph.facebook.com/v3.3/".$page_id."/ratings?access_token=".$page_access;

    //    $response_page_ratings = file_get_contents($url_get_ratings);
    //   return $resp_ratings = json_decode($response_page_ratings, true);

})->name('facebook.test');


Route::get('/facebook/test2', function(){
    return request();
    $url = "https://graph.facebook.com/oauth/access_token?client_id=".env('FACEBOOK_ID')."&client_secret=".env('FACEBOOK_SECRET')."&code=".request('code')."&redirect_uri=".route('facebook.test');
    return redirect($url);
})->name('facebook.test2');

//review controller
    // Route::resource('review','ReviewController');
Route::group(['prefix' => 'reviews'], function(){
    Route::get('/', 'ReviewController@showRandomReviews')->name('review.show-random');
    Route::get('/submitted', 'ReviewController@submitted')->name('review.submitted');
    Route::get('/write/{realtor?}', 'ReviewController@create')->name('review.create');

    Route::post('/submit', 'ReviewController@store')->name('review.store');
    // Route::post('/submit', 'ReviewController@store')->name('review.store');
    Route::get('/search', 'ReviewController@search')->name('review.search');

});


//reply review authenticated
Route::group(['prefix' => 'reply','middleware' => 'session_auth'], function(){
    Route::post('/store/{id}', 'ReplyController@store')->name('user.reply.store');
    Route::get('/submitted', 'ReviewController@submitted')->name('reply.submitted');
    Route::get('/write/{realtor?}', 'ReviewController@create')->name('review.create');

});
Route::get('/search', 'ReviewController@search')->name('review.search');



//authenticated user dashboard
Route::group(['prefix' => 'user', 'middleware' => 'session_auth'], function(){
    Route::get('/dashboard', 'UserController@dashboard')->name('user.dashboard');
    Route::get('/token/refresh/{url}', 'UserController@tokenRefresh')->name('user.token.refresh');
    Route::post('/upload-avatar', 'UserController@uploadAvatar')->name('user.upload-avatar');
    //contact controller
    Route::group(['prefix' => 'contacts'], function (){
       Route::get('/', 'ContactController@index')->name('user.contacts');
       Route::post('/get', 'ContactController@search')->name('user.contacts.search');
       Route::post('/store', 'ContactController@store')->name('user.contact.store');
       Route::get('/delete/{id}', 'ContactController@destroy')->name('user.contact.delete');
       Route::post('/update', 'ContactController@update')->name('user.contact.update');
       Route::get('/bulk-upload/format', 'ContactController@getTemplateFormat')->name('contact.bulk-upload.format');
    });

    //Reviews
    Route::group(['prefix' => 'reviews'], function (){
        Route::get('/', 'ReviewController@index')->name('user.reviews');

    });


    //payment controller
    Route::group(['prefix' => 'payment'], function () {
        Route::post('/payment-method/store', 'PaymentController@store')->name('payment.method.add');
        Route::get('/payment-method/create/{payment_plan}', 'PaymentController@create')->name('payment.method.create');
    });


    //pricing plans
   Route::get('/plans','PackageController@index')->name('packages');
   Route::get('/plans/{package_id}','PackageController@show')->name('package.preview');




    Route::get('/integrations', 'IntegrationController@index')->name('user.integrations');

    Route::get('/profile', 'UserController@profile')->name('user.profile');

    Route::get('integration/create/{id}/{type}', 'IntegrationController@store')->name('user.integration.store');

    //change password
    Route::post('/auth/change-password', 'UserController@changePassword')->name('auth.change-password');

    Route::get('/invitations', 'InvitationController@index')->name('user.invitations');
    Route::post('/store', 'InvitationController@store')->name('user.invitation.store');

});

Route::group(['prefix' => 'user/invitations'], function (){

    Route::get('/show/{code}', 'InvitationController@show')->name('user.invitation.show');

});

Route::get('/u/{business_name}', 'UserController@index')->name('user.view');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/login/{provider}', 'SocialController@redirectToProvider')->name('socialLogin');
Route::get('/auth/{provider}/callback', 'SocialController@handleProviderCallback');



Route::get('/stripe',function(){
    $url = 'user/stripe';
    $client = \App\Library\Consume::getInstance();
    $response =  $client->getResponse('get',$url);

    if($response['status']  == 1){
        $intent = $response['data'];
        return view('user.stripe', compact('intent'));
    }
    return throwError($response);
});
///integration



