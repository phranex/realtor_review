<?php

namespace App\Providers;


use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Library\Consume;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        view()->composer(['layouts.master'], function($view){
            $contact = null;
            $client = Consume::getInstance();
            $url = 'page-setup/contact';
            $response = $client->getResponse('get',$url);
            logger('hello');
            if($response['status'] == 1){
                $contact = (array) $response['data']->setting;
            }
        $view->with(['contact' => $contact]);
    });


    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer('layouts.main', function($view){
            $contact = null;
            $client = Consume::getInstance();
            $url = 'page-setup/contact';
            $response = $client->getResponse('get',$url);
            logger('hello');
            if($response['status'] == 1){
                $contact = (array) $response['data']->setting;
            }
        $view->with(['contact' => $contact]);
    });

    }
}
