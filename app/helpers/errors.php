<?php

    function throwError($response){
        if(isset($response['status_code'])){
            $bad_token_requests_code = [401,400,403];
            if(in_array($response['status_code'],$bad_token_requests_code)){
                session(['login'=>true]);
                session(['redirect_url' => url()->previous()]);
                logger($response);
                userLogout();
                return redirect()->route('landing-page')->with('error', __('Please Log in to continue'));
            }elseif($response['status_code'] == 500){
                logger($response);
                return back()->with('error',__('An internal Server error occurred'));
                // throw new Exception("Server Error", 500);
            }elseif($response['status_code'] == 411){
                logger($response);
                session(['login'=>true]);
                //session(['redirect_url' => url()->previous()]);
                userLogout();
                return redirect()->route('landing-page')->with('error', __('Invalid credentials'));
            }elseif($response['status_code'] == 200){
                logger($response);
                return back()->with('error',__($response['message']));
            }


        }
        logger($response);
        return back()->with('error',__('Error Occurred'));
        // throw new Exception("Error occurred", 520);

    }

?>
