<?php
use \App\Library\Consume;

function formatDate($date,$format){
    echo \Carbon\Carbon::parse($date)->format($format);
}

function displayPhoto($link){
    if(!empty($link))
        return $link;

    return asset('img/avatar1.jpg');
}
function when($time){
    echo \Carbon\Carbon::parse($time)->diffForHumans();
}

function currencyConverter($amt){
    echo '$'. $amt;
}

function rating($review){
    if($review){
        // if(gettype($review))
        //     $review_ = $review['rating_score'];
        // else
        //     $review_ = $review->rating_score;
        for($i = 0; $i < $review;  $i++){

                $j = $i + 0.5;
                if($j == $review){
                    echo "<i class='icon_star half-star'></i>";
                    break;
                }else{
                    echo "<i class='icon_star'></i>";
                }
        }
        $review_ = ceil($review);

        for($i = 0; $i < 5 - $review_;  $i++){
            echo "<i class='icon_star empty'></i>";
        }
        echo "<em>".number_format($review, 2)."/5.00</em>";
    }



}

function register($data)
{
    # code...
    $consume = new Consume();

        $url = 'auth/register/social';
        $res = $consume->getResponse('POST',$url, $data);

        if($res['status']){
            session()->forget('register');
            //save session

            session(['access_token' => $res['data']->access_token]);
            //redirect to preferences
            session(['user' => $res['data']->user]);
            session(['user_subscription' => $res['data']->subscription]);
            return redirect()->route('user.dashboard')->with('success', __('Login was successful.'));

        }
        return throwError($res);

}

?>
