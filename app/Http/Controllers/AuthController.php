<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Library\Consume;



class AuthController
{

    public function register(Request $request)
    {
           # code...

        session(['register'=>true]);

        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'business_name' => 'required',
            'email' => 'email|required',
            'password' => 'string|confirmed|min:6',
        ]);


        //register via api
        $data = request()->all();
        $consume = new Consume();

        $url = 'auth/register';
        $res = $consume->getResponse('POST',$url, $data);

        if($res['status']){
            session()->forget('register');
            //save session
            session(['access_token' => $res['data']->access_token]);
            session(['user' => $res['data']->user]);
            session(['user_subscription' => $res['data']->subscription]);
            //redirect to preferences
            return redirect()->route('user.dashboard')->with('success', __('Registration was successful. Please check your mail to activate your account'));

        }
        return throwError($res);

    }

    public function login(Request $request){
        session(['login'=>true]);
        userLogout();
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $client = Consume::getInstance();

        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];
        $url = 'auth/login';
        $response = $client->getResponse('POST',$url,$data);

        if($response['status']){
            session()->forget('login');
            //save sessi
            session(['access_token' => $response['data']->access_token]);
            session(['user' => $response['data']->user]);
            session(['user_subscription' => $response['data']->subscription]);
            // session(['identifier' => $response['data']->identifier]);
            $business_name = session('user.business_name');
            if(!empty($response['data']->business_name)){
                return redirect()->route('user.dashboard')->with('success', __('Login was successful.'));
                // return redirect()->route('user.view',$response['data']->business_name)->with('success', __('Login was successful.'));
            }
            //dd($user);
            //redirect to dashboard
            return redirect()->route('user.profile')->with('success', __('Login was successful.'));

        }


        return throwError($response);

    }
}
