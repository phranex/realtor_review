<?php

namespace App\Http\Controllers;

use App\Library\Consume;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($payment_plan)
    {
        //


        $url = 'user/stripe';
        $client = \App\Library\Consume::getInstance();
        $response =  $client->getResponse('get',$url);
        if($response['status']  == 1){
            $intent = $response['data'];
            return view('user.stripe', compact('intent','payment_plan'));
        }
        return throwError($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'payment_method' => 'required',
            'payment_plan'  => 'required',
        ]);

        $plan = explode('-',base64_decode($request->payment_plan));
        $plan_name =  $plan[0];
        $plan_id = $plan[1];

        $url = 'user/payment-method/store';
        $client = Consume::getInstance();
        $data = [
            'payment_method' => $request->payment_method,
            'plan' => $plan_name,
            'plan_id' => $plan_id,
        ];
       $response = $client->getResponse('post',$url, $data);
        if($response['status'] == 1){
            return redirect()->route('user.dashboard')->with('success', 'Your account has been upgraded.');
        }
        return throwError($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
