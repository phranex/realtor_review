<?php

namespace App\Http\Controllers;
use Socialite;

use Illuminate\Http\Request;

class SocialController extends Controller
{
    //
    //
    protected $redirectTo = '/user/dashboard';

    public function redirectToProvider($provider)
    {
        // if(!empty(request('p'))){
        //     session(['ur' => request('p')]);
        // }
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        try{
            if(!empty(request('error_description'))){
                $des = request('error_description');
                return redirect(route('register', "r=$des"));
            }
            if($provider == 'twitter')
                $user = dd(Socialite::driver($provider)->user());
            else
                $user = Socialite::driver($provider)->stateless()->user();
            return $this->findOrCreateUser($user, $provider);

        }
        catch(Exception $e){
            return redirect(route('landing-page'))->with('error', "Unexpected error occurred while connecting to $provider");
        }


        // $user->token;
    }

     /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $user = $user->user;
        session(['register'=>true]);


        $user_id = ($user['id'])?$user['id']:$user->id;
        // $authUser = User::where('provider_id', $user_id)->where('provider', $provider)->first();
        if($provider == 'facebook'){
            $email = $user['email'];
            $name = explode(' ', $user['name']);
            $firstName = $name[1];
            $lastName = $name[0];
            $data = [
                'first_name' => $firstName,
                'last_name' => $lastName,
                'email' => $email,
                'password' => $user_id
            ];

            // $id = $user->id;
            return register($data);

        }else if($provider == 'twitter'){
            // $authEmail = User::where('email', $user->email)->first();
            $email = $user->email;
            $name = explode(' ', $user->user['name']);
            $firstName = $name[1];
            $lastName = $name[0];
            $data = [
                'first_name' => $firstName,
                'last_name' => $lastName,
                'email' => $email
            ];
            // $id = $user->id;
            return register($data);


        }else if($provider == 'google'){
           //dd($user);
            // $authEmail = User::where('email', $user['emails'][0]['value'])->first();
            $email = $user['emails'][0]['value'];
            $firstName =  $user['name']['givenName'];
            $lastName = $user['name']['familyName'];

            $data = [
                'first_name' => $firstName,
                'last_name' => $lastName,
                'email' => $email
            ];
            // $id = $user->id;
           return  register($data);
            // $id = $user['id'];
        }else{
            // $authEmail = User::where('email', $user['emailAddress'])->first();
            $email = $user['emailAddress'];
            $firstName = $user['firstName'];
            $lastName = $user['lastName'];

            $data = [
                'first_name' => $firstName,
                'last_name' => $lastName,
                'email' => $email
            ];
            // $id = $user->id;
            return register($data);
            // $user_type = 1;
            // $id = $user['id'];

        }

}

    public function createUser($email,$provider,$firstName, $lastName,$user_type,$id){


       return $new_user = User::create([
            // 'firstName'     => $name[0],
            // 'lastName'     => $name[1],
            'username'    => str_replace(' ', '_',strtolower($firstName.$lastName.rand(1,100))),
            'email'    => $email,
            'provider' => $provider,
            'userType' => $user_type,
            // 'linkedin' => @$user['publicProfileUrl'],
            'provider_id' => $id,
            'verifyToken' => \Illuminate\Support\Str::random(40),
            'verified' => 0,
        ]);
    }


}
