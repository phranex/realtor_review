<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Library\Consume as AppConsume;

class PackageController extends Controller
{
    //

    public function index()
    {
        # code...
        $client = AppConsume::getInstance();
        $url = 'packages/get';
       $response = $client->getResponse('get',$url);
        if($response['status'] == 1){
            $packages = $response['data'];

            return view('user.package', compact('packages'));

        }
        return throwError($response);
    }

    public function show($package_id)
    {
        # code...
        if(!is_numeric($package_id)) return back()->with('error', 'An unexpected error occurred');
        $client = AppConsume::getInstance();
        $url = 'packages/get/'.$package_id;
       $response = $client->getResponse('get',$url);
        if($response['status'] == 1){
            $package = $response['data'];

            return view('user.package-preview', compact('package'));
        }
       return throwError($response);
    }
}
