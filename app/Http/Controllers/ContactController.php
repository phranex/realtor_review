<?php

namespace App\Http\Controllers;

use App\Contact;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Library\Consume;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client_ = Consume::getInstance();
        $url_ = 'user/templates';
        $response_ = $client_->getResponse('get',$url_);
        $templates = (array) @$response_['data'];

        if(!empty(request('filter'))){

            $client = Consume::getInstance();
            $url = 'user/contact/filter/get?filter='.request('filter');
           $response = $client->getResponse('get',$url);

            if($response['status'] == 1){
                $contacts = (array) $response['data'];

                return view('contact.index', compact('contacts','templates'));

            }
             return throwError($response);
        }

        if(!empty(request('query'))){
            request()->validate([
                'query' => 'required',
            ]);

            $client = Consume::getInstance();
            $url = 'user/contact/search/get?query='.request('query');
            $response = $client->getResponse('get',$url);

            if($response['status'] == 1){
                $contacts = (array) $response['data'];

                return view('contact.index', compact('contacts'));

            }
             return throwError($response);
        }
        $client = Consume::getInstance();
        $url = 'user/contact/index';
        $response = $client->getResponse('get',$url);



        if($response['status']){
            $contacts = (array) $response['data'];

            return view('contact.index', compact('contacts','templates'));
        }
         return throwError($response);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'fullname' => 'required',
            'email' => 'email|required',
            'phone_number' => 'numeric'
        ]);
        $data = request()->except(['_token']);

        $client = Consume::getInstance();
        $url = 'user/contact/store';
        $response = $client->getResponse('post',$url,$data);

        if($response['status']){
            return redirect()->route('user.contacts')->with('success', __('Contact Created') );
        }
         return throwError($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $request->validate([
            'fullname' => 'required',
            'email' => 'email|required',
            'phone_number' => 'numeric'
        ]);
        $data = request()->except(['_token']);

        $client = Consume::getInstance();
        $url = 'user/contact/update';
        $response = $client->getResponse('post',$url,$data);

        if($response['status']){
            return back()->with('success', __('Contact Created') );
        }
         return throwError($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $client = Consume::getInstance();
        $url = 'user/contact/delete/'.$id;
        $response = $client->getResponse('post',$url);

        if($response['status']){
            return back()->with('success', __('Contact Deleted') );
        }
         return throwError($response);
    }

    public function getTemplateFormat()
    {
        # code...
        //
        $client = Consume::getInstance();
        $url = 'user/contact/get-bulk-format/';
        return $response = $client->getResponse('get',$url);

        if($response['status']){
            return response()->json([
                'status' => 1,
                'message' => 'success',
                'data' => $response['data']
            ]);;
        }

        return response()->json([
            'status' => 0,
            'message' => 'error',
            'data' => $response['data']
        ]);;


    }

    public function search()
    {

        # code...

    }
}
