<?php

namespace App\Http\Controllers;

use App\r;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Auth;
use App\Library\Consume;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('session_auth')->except(['index']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($business_name)
    {
        //
        $url = 'user/'.$business_name.'/profile';
        $client = Consume::getInstance();
        $response = $client->getResponse('get',$url);
        if($response['status']){
            $user = $response['data'];
            $num_of_reviews = count($user->reviews);
            $total_rating_score = 0;
            if($num_of_reviews){
                $total_rating_score = 0;
                for($i = 0; $i < $num_of_reviews; $i++){
                    $total_rating_score += $user->reviews[$i]['rating_score'];
                }
                $total_rating_score = round($total_rating_score / $num_of_reviews);
            }

            // dd($user);
            if(session()->has('user')){
                $business_id = $user->id;
                $is_user = false;
                $logged_in_user_id = session('user.id');
                if($business_id == $logged_in_user_id)
                    // $is_user = true;
                    return view('user.index', compact('user'));
                else
                    return view('user.profile-reviews', compact('user','total_rating_score','num_of_reviews'));
            }else{

                return view('user.profile-reviews', compact('user','total_rating_score','num_of_reviews'));
            }

        }
         return throwError($response);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function show(r $r)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function edit(r $r)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, r $r)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function destroy(r $r)
    {
        //
    }


    public function dashboard(){
        $client = Consume::getInstance();
        $url = 'reviews/integrations/get';
        $response = $client->getResponse('get',$url);
        $ratings = null;

        $url_user = 'user/dashboard/get';
        $response_user = $client->getResponse('get',$url_user);

        if($response_user['status'] == 1){
            if($response['status'] == 1)
            $ratings =  $response['data']->ratings;
            session(['user_subscription' => $response_user['data']->subscription]);
            $analytics = $response_user['data'];
            return view('user.dashboard', compact('ratings','analytics'));
        }

        return throwError($response_user);

    }

    public function profile(){
        $client = Consume::getInstance();
        //user info
        $url = 'auth/me';
        $response = $client->getResponse('POST',$url);


        //social link types
        $url = 'social/get-types';
        $response2 = $client->getResponse('GET',$url);

        $socialTypes = null;

        if($response['status']){
            $user = $response['data']; //user data

            $profile = (object) $user->profile; //profile data
            $socials = (array) $user->social; //profile data

            if($response2['status']){
                $socialTypes = (array) $response2['data']; //social types available
                // dd($socialTypes);
            /*
                Check if user has created a social link. If he has,
                remove the social link type he created from the social link types available.
            */
            if(isset($socials)){
                $social_types = [];
                foreach ($socials as $social) {
                    # code...
                    array_push($social_types,$social['socialLinkType_id']);
                }
                foreach ($socialTypes as $key => $value) {
                    if(in_array($value['id'],$social_types))
                        unset($socialTypes[$key]);
                }
            }


            return view('user.profile', compact('user', 'profile','socialTypes','socials'));
        }

    }
        return throwError($response);

    }

    public function profilestore(Request $request){

        $request->validate([
            'title' => 'required',
            'phone_number' => 'required',
            'email' => 'required',
            'business_name' => 'required',
            'address' => 'required',
            'date_of_birth' => 'required',
            // 'city' => 'required',
            // 'state' => 'required',
            // 'country' => 'required',
            // 'zip' => 'required'
        ]);
        //

        $client = Consume::getInstance();
        $url = 'user/personal-settings/store';
        $data = request()->all();
        $response = $client->getResponse('POST',$url,$data);

        if($response['status']){
            return redirect()->route('user.profile')->with('success', __('Personal settings updated successfully'));
        }

        return throwError($response);
    }

    public function socialStore(Request $request){
        $data = request()->except(['_token']);
        // dd($data);
        $client = Consume::getInstance();
        $url = 'social/store';
        $response = $client->getResponse('POST', $url,$data);
    //    dd($response);
        if($response['status']){
            return back()->with('success', __('Social Links Updated') );
        }
         return throwError($response);
    }

    public function tokenRefresh($url)
    {
        # code...
        $client = Consume::getInstance();
        $url = 'auth/me';
        $user = $client->getResponse('POST',$url);
    }

    public function uploadAvatar(Request $request)
    {
        # code...
        $data = request()->except(['_token']);
        $client = Consume::getInstance();
        $url = 'user/upload-avatar';
        $file = [
            'name' => 'avatar',
            'contents' => fopen(request()->file('avatar'), 'r')
        ];
        $response = $client->uploadFile($url,$file);

        if($response['status']){
            return back()->with('success', __('Avatar Uploaded') );
        }
         return throwError($response);
    }

    public function changePassword(Request $request)
    {
        // if(auth()->user()->blocked)
        // return redirect(route('user.blocked'));
        $this->validate($request, [
            "old_password" => "required",
            "password" => "required|confirmed",
        ]);
        $old = request('old_pass');

        $client = Consume::getInstance();
        $url = 'auth/change-password';
        $data = [
            'old_password' => request('old_password'),
            'password' => request('password')
        ];
        $response = $client->getResponse('post',$url,$data);
        if($response['status'] == 1){

            userLogout();
            session(['login' => true]);
            return redirect()->route('landing-page')->with('success', trans('Your password changed. Login to continue'));
        }elseif($response['status'] == 56){
            return back()->with('error',trans('Your password is incorrect'));
        }

        return back()->With('error',trans('An unexpected error occurred. Please try again'));
    }
}
