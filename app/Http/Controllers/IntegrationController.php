<?php

namespace App\Http\Controllers;

use App\Integration;
use Illuminate\Http\Request;
use \App\Library\Consume;

class IntegrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $client = Consume::getInstance();
        $url = 'integrations/types';
        $response = $client->getResponse('get',$url);

        $url_integrations = 'user/integrations';
        $response_integrations = $client->getResponse('get',$url_integrations);



        if($response['status'] == 1 && $response_integrations['status'] == 1 ){
            $user_integrations = (array) $response_integrations['data']->integrations;
             $identifier = $response_integrations['data']->identifier;
            $integrations = (array) $response['data'];

            return view('integration.create',compact('user_integrations','integrations', 'identifier'));
        }
         return throwError($response);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id,$type)
    {
        //
        if(is_numeric($id)){
            if(session()->has('fallback')){
                if(session()->has('user_fb_pages')){
                    $pages = session('user_fb_pages');
                    $page_data = array();
                    if(count($pages)){
                        foreach($pages as $page){
                            $obj['external_id'] = $page['id'];
                            $obj['name'] = $page['name'];
                            $obj['access_token'] = $page['access_token'];
                            $obj['third_party_integration_id'] = $id;

                            array_push($page_data, (object) $obj );
                        }

                        $client = Consume::getInstance();
                        $url = 'user/integrations/store';
                        $response = $client->getResponse('post',$url,$page_data);
                        if($response['status'] == 1){
                            session()->forget(['user_fb_pages','fallback']);
                            return redirect()->route('user.integrations')->with('success', 'Integrated Successfully');
                        }else{
                            session()->forget(['user_fb_pages','fallback']);
                            return redirect()->route('user.integrations')->with('error', $response['message']);
                        }

                        return throwError($response);

                    }




                }
            }
            if(strtolower($type) == 'facebook'){
                session(['fallback' => url()->current()]);

                return redirect()->route('facebook.integration');
            }else if(strtolower($type) == 'zillow'){

            }
            return back()->with('error', 'Please select a third party');
        }

        return back()->with('error', 'An expected error occurred');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Integration  $integration
     * @return \Illuminate\Http\Response
     */
    public function show(Integration $integration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Integration  $integration
     * @return \Illuminate\Http\Response
     */
    public function edit(Integration $integration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Integration  $integration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Integration $integration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Integration  $integration
     * @return \Illuminate\Http\Response
     */
    public function destroy(Integration $integration)
    {
        //
    }
}
