<?php

namespace App\Http\Controllers;

use App\Review;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use \App\Library\Consume;

class ReviewController extends Controller
{
    public function __construct()
    {

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = Consume::getInstance();
        $url = 'reviews';
        $response = $client->getResponse('get',$url);
        if($response['status']){
            $reviews = $response['data'];
            dd($reviews);
            return view('review.index',compact('reviews'));
        }
        return throwError($response);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($realtor)
    {
        //

        if(!empty($realtor)){
            $request = base64_decode($realtor);
            $data = explode('-',$request);

// dd($data);
            if(session()->has('user')){
                $logged_user = session('user.id');
                if($logged_user == $data[1]) return back()->with('error', 'You can not review yourself');
            }

            return view('review.create',compact('data'));
        }
        return back()->with('error', 'An unexpected error occurred');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required',
            'review' => 'required',
            'rating_score' => 'numeric',
            'user_id' => 'numeric'
        ]);
        // dd($request);

        $client = Consume::getInstance();
        $url = 'review/store/save';
        // if(empty(request('invitation_id')))
        //     $url = 'user/review/store/anonymous';
        // else {
        //     $url = 'user/review/store/';
        // }
        $data = request()->except(['_token']);

        $response = $client->getResponse('post',$url,$data);

        if($response['status']){
            return redirect()->route('review.submitted');
        }
        return throwError($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        //
    }

    public function showRandomReviews(){
        $client = Consume::getInstance();
        $url = 'reviews';
        $response = $client->getResponse('get',$url);
        if($response['status']){
            $reviews = (array) $response['data'];
            // dd($reviews);
            return view('review.random',compact('reviews'));
        }
        return throwError($response);

    }

    public function submitted(){
        return view('review.submitted');
    }

    public function  search(Request $request){
        if(request('realtor')){
            $query = $request->realtor;
            $client = Consume::getInstance();
            $url = 'search/realtor?query='.$query;
            $response = $client->getResponse('get', $url);
            if($response['status']){
                $results = (array) $response['data'];

                return view('review.search-result',compact('results'));
            }
            return throwError($response);
        }
        return back()->with('error', 'Please enter realtor\'s business name');

    }
}
