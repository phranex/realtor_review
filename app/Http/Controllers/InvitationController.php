<?php

namespace App\Http\Controllers;

use App\Invitation;
use Illuminate\Http\Request;
use \App\Library\Consume;
class InvitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all invitations
        $client = Consume::getInstance();
        $url = 'user/invitation';
        $response = $client->getResponse('get',$url);

        if($response['status']){
            $invitations = (array) $response['data'];

            return view('invitation.index', compact('invitations') );
        }
         return throwError($response);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        // dd(request()->all());
        if(request('template')){
            $request->validate([
                'message' => 'required'
                ]);
                $new = false;
        }else{
            $request->validate([
                'message' => 'required',
                'template_name' => 'required',
            ]);
            $new = true;
        }

        // if(request('emails') == null){
        //     $request->validate([
        //         'all' => 'required'
        //     ]);
        //     $all_contacts = true;
        // }else{
        //     $all_contacts = false;
        // }
        // $data = ['all' => $all_contacts, 'message' => $request->message, 'emails' => explode(',',$request->emails)];
        $data = ['new' => $new,'template' => @$request->template, 'template_name' => @$request->template_name, 'message' => $request->message, 'emails' => explode(',',$request->emails)];


        $client = Consume::getInstance();
        $url = 'user/invitation/store';
        $response = $client->getResponse('post',$url,$data);

        if($response['status']){
            return back()->with('success', $response['message'] );
        }
         return throwError($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function show($invitation_code)
    {
        //
        $decoded_code = base64_decode($invitation_code);
        $string = str_replace('_',',',$decoded_code);
        $arr = explode(',',$string);
        $email = $arr[0];
        //get invitation data
        $client = Consume::getInstance();
        $url = 'user/invitation/showByCode/'.$invitation_code;
        $response = $client->getResponse('get',$url);

        if($response['status'] == 1){
            $invitation = $response['data'];

            return view('review.create', compact('invitation','email','invitation_code'));
        }
        if($response['status'] == 56){
            return redirect()->route('landing-page')->with('error', 'Invitation link has expired or you have sent your review');
        }
         return throwError($response);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function edit(Invitation $invitation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invitation $invitation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invitation $invitation)
    {
        //
    }
}
