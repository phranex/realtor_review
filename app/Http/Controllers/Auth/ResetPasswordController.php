<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\Library\Consume;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function reset(Request $request)
    {

        request()->validate([
            'email' => 'required',
            'password' => 'required|confirmed|min:6'
        ]);
        $data = request()->all();
        $url = 'auth/reset';
        $client = Consume::getInstance();
        $response = $client->getResponse('post',$url,$data);
        if($response['status'] == 1){
            session(['login' => true]);
            return redirect()->route('landing-page')->with('success',trans('Password successfully changed. Log in to continue'));
        }else if($response['status'] == 56){
            return back()->withInput(request()->all())->with('error', __($response['message']));

        }

        return throwError($response);

        return back()->with('error', trans($response['message']));


    }
}
