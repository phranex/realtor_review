<?php

namespace App\Http\Middleware;

use Closure;

class SessionAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return $next($request);
        if(!session()->has('access_token')){
            session(['login'=>true]);
            return redirect()->route('landing-page');
        }
        return $next($request);
    }
}
